package com.evildeedz.survivalwars;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.WorldBorder;
import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;


public class Arena{

	private int id;
	private ArrayList<UUID> players;
	private Location spawn;
	private GameState state;
	private Countdown countdown;
	private ItemStack netherStar;
	private Game game;
	private boolean canJoin;
	private Location sign;
	private WorldBorder border;
	private float borderSize;
	private float borderSubtract;
	//Hashmaps
	private HashMap<UUID, Integer> isAlive; // 1 = Alive, 0 = Dead
	private HashMap<UUID, Integer> kills;
	private HashMap<UUID, String> kits;
	private HashMap<UUID, Long> cooldowns;
	
	public Arena(int id) {
		this.id = id;
		players = new ArrayList<>();
		spawn = Config.getArenaSpawn(id);
		state = GameState.RECRUITING;
		countdown = new Countdown(this);
		game = new Game(this);
		sign = Config.getArenaSign(id);
		isAlive = new HashMap<>();
		kills = new HashMap<>();
		kits = new HashMap<>();
		cooldowns = new HashMap<>();
		System.out.println("Arena" + id + " spawn is " + spawn);
		canJoin = true;
		
		// World border
		
		border = spawn.getWorld().getWorldBorder();
		borderSize = 3000;
		borderSubtract = borderSize/700;
		border.setSize(borderSize);
		border.setCenter(spawn.getX(), spawn.getZ());
		border.setDamageAmount(3);
		border.setWarningDistance(1);
		// Sign Update
		updateSign(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "=>ARENA " + id + "<=", ChatColor.GREEN + "Recruiting", ChatColor.BLACK.toString() + players.size() + "/20", ChatColor.GREEN + "Click To Join");
	}
	
	public void Start()
	{
		for(UUID uuid: players)
		{
			Bukkit.getPlayer(uuid).closeInventory();
			Bukkit.getPlayer(uuid).getInventory().clear();
		}
		game.start();
		
	}	
	
	public void Reset()
	{
		for(UUID uuid: players) {
			Player player = Bukkit.getPlayer(uuid);
			callsForPlayerRemove(player);
		}
		
		players.clear();
		isAlive.clear();
		kills.clear();
		kits.clear();
		cooldowns.clear();
		countdown = new Countdown(this);
		
		border.setCenter(spawn.getX(), spawn.getY());
		if(!(state == GameState.RECRUITING) && !(state == GameState.COUNTDOWN))
		{
			
			canJoin = false;
			updateSign(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "=>ARENA " + id + "<=", ChatColor.RED + "Resetting", ChatColor.BLACK.toString() + players.size() + "/20", ChatColor.RED + "Cant Join");
			
			Bukkit.unloadWorld(spawn.getWorld().getName(), false);
			
		/*	Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable(){ // Run It Later to make sure its unloaded.
			    @Override
			    public void run() {
			    	final File worldFolder = new File(spawn.getWorld().getName()); // World folder name
			        deleteFolder(worldFolder); // Delete old folder
			    }
			}, 60L);*/
			
			Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable(){ // Run It Later to make sure its unloaded.
			    @Override
			    public void run() {
				    spawn = Config.getArenaSpawn(id);
			    }
			}, 60L);
		}
		
		state = GameState.RECRUITING;
		game = new Game(this);
		borderSize = 3000;
		border.setSize(borderSize);
	}
	
	public void sendMessage(String message)
	{
		for (UUID uuid: players)
		{
			Bukkit.getPlayer(uuid).sendMessage(message);
		}
	}
	
	public void playSound(Sound sound)
	{
		for(UUID uuid: players)
		{
			Bukkit.getPlayer(uuid).playSound(Bukkit.getPlayer(uuid).getLocation(), sound, 1, 1);
		}
	}
	
	// ADD PLAYER TO ARENA
	public void addPlayer(Player player)
	{
		if(players.size() < Config.getMaxPlayers())
		{
			players.add(player.getUniqueId());
			isAlive.put(player.getUniqueId(), 1);
			kills.put(player.getUniqueId(), 0);
			kits.put(player.getUniqueId(), "none");
			cooldowns.put(player.getUniqueId(), (long) 0);
			player.teleport(spawn);	
			player.sendMessage(ChatColor.GREEN + "Joined Arena " + ChatColor.RED + getID() + ChatColor.GREEN + "!");
			player.getInventory().clear();
			player.setHealth(20);
			player.setFoodLevel(20);
			netherStar = new ItemStack(Material.NETHER_STAR);
			ItemMeta netherStarMeta = netherStar.getItemMeta();
			netherStarMeta.setDisplayName(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "Kit Selector");
			netherStar.setItemMeta(netherStarMeta);
			player.getInventory().addItem(netherStar);
			CreateBoard(player);
			updateScoreboardPlayers();
			sendMessage(ChatColor.GREEN + player.getName() + " has joined the game.");
			// Remove all mobs if its the first player to join

			for(Entity e: player.getWorld().getEntities())
			{

				if(!(e instanceof Player))
				{
					e.remove();
				}
			}

			
			// Update Sign
			if(state == GameState.RECRUITING ||state == GameState.COUNTDOWN)
			{
				if(players.size() == Config.getMaxPlayers())
				{	
					updateSign(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "=>ARENA " + id + "<=", ChatColor.GREEN + "RECRUITING", ChatColor.BLACK.toString() + players.size() + "/20", ChatColor.RED + "Lobby Full");
				}else
				{
					updateSign(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "=>ARENA " + id + "<=", ChatColor.GREEN + "RECRUITING", ChatColor.BLACK.toString() + players.size() + "/20", ChatColor.GREEN + "Click To Join");
				}
			}
		}else {
			player.sendMessage(ChatColor.RED + "Can't join, lobby is full!");
		}	
		
		// Start countdown
		if(players.size() >= Config.getRequiredPlayers() && state == GameState.RECRUITING)
		{
			countdown.begin();
		}
		

	}
	
	// REMOVE PLAYER FROM ARENA
	public void removePlayer(Player player)
	{
		int aliveState = getPlayerAliveStatus(player);
		String kitName = getKitName(player);
		
		players.remove(player.getUniqueId());
		callsForPlayerRemove(player);
		updateScoreboardPlayers();
		
		// Sending Custom Message on player remove
		if(state == GameState.PRELIVE && aliveState == 1 || state == GameState.LIVE && aliveState == 1)
		{
			sendMessage(ChatColor.DARK_RED + player.getName() + "(" + kitName + ") " + ChatColor.RED + "has left the game." );
			playSound(Sound.ENTITY_LIGHTNING_BOLT_THUNDER);
		}
		else if(state == GameState.RECRUITING || state == GameState.COUNTDOWN)
		{
			sendMessage(ChatColor.RED + player.getName() + " has left the game.");
		}
		// If on removal the player size becomes 0 at any point we reset the arena
		if(players.size() == 0 && state != GameState.ENDING)
		{
			Reset();
		}
		//Updating Players in arena on Sign when someone leaves
		if(state == GameState.COUNTDOWN || state == GameState.RECRUITING)
		{
			updateSign(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "=>ARENA " + id + "<=", ChatColor.GREEN + "RECRUITING", ChatColor.BLACK.toString() + players.size() + "/20", ChatColor.GREEN + "Click To Join");
		}
		else if(state == GameState.PRELIVE || state == GameState.LIVE)
		{
			updateSign(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "=>ARENA " + id + "<=", ChatColor.GREEN + "ONGOING", ChatColor.BLACK.toString() + getPlayersAlive() + "/20", ChatColor.RED + "Can't Join");
		}
		else if(state == GameState.ENDING)
		{
			updateSign(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "=>ARENA " + id + "<=", ChatColor.RED + "ENDING", ChatColor.BLACK.toString() + getPlayersAlive() + "/20", ChatColor.RED + "Can't Join");
		}
		
		// If on removal the player size becomes 1 and game is ongoing, we change state
		if(players.size() == 1 && state == GameState.LIVE || players.size() == 1 && state == GameState.PRELIVE)
		{
			game.Ending();
		}
	}
	
	// PLAYER REMOVE CALLS FOR BOTH RESET & REMOVEPLAYER METHOD
	public void callsForPlayerRemove(Player player)
	{
		//if Player is alive and gamestate is prelive/ live/ending drop its inventory on death
		if(getPlayerAliveStatus(player) == 1)
		{
			if(state == GameState.PRELIVE || state == GameState.LIVE || state == GameState.ENDING)
			{
				try
	            {
	                for (ItemStack itemStack : player.getInventory().getContents()) {
	                    player.getWorld().dropItemNaturally(player.getLocation(), itemStack);
	                    player.getInventory().removeItem(itemStack);
	                }
	                for (ItemStack itemStack : player.getInventory().getArmorContents()) {
	                    player.getWorld().dropItemNaturally(player.getLocation(), itemStack);
	                    player.getInventory().removeItem(itemStack);
	                }
	            }
	            catch(IllegalArgumentException e){ }
			}
			else {	player.getInventory().clear();	}
		}
		else{	player.getInventory().clear();	}
		
		isAlive.remove(player.getUniqueId());
		cooldowns.remove(player.getUniqueId());
		player.teleport(Config.getLobbySpawn());
		player.setHealth(20);
		player.setFoodLevel(20);
		player.setAllowFlight(false);
		player.setFlying(false);
		player.setInvisible(false);
		// Unvanishing the player
		player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		for(Player target: Bukkit.getOnlinePlayers())
		{
			target.showPlayer(Main.getInstance(), player); 
		}
		// Removing Potion effects
		for (PotionEffect effect : player.getActivePotionEffects())
	        player.removePotionEffect(effect.getType());
		player.getInventory().clear();
	}
	
	// ISALIVE STATUS
	public int getPlayersAlive()
	{
		// Returns the number of players that are alive
		int playersAlive = 0;
		for(int i: isAlive.values())
		{
			if(i == 1)
			{
				playersAlive+=1;
			}
		}
		return playersAlive;
	}
	
	public void setPlayerDead(Player player)
	{
		// Only to be called when player is killed, because if player leaves then 
		// there Record will be deleted anyway
		isAlive.replace(player.getUniqueId(), 0);
		updateScoreboardPlayers();
		// Checking everytime a player dies whether the game players alive is 1 or not
		if(getPlayersAlive() == 1 && state == GameState.LIVE)
		{
			game.Ending();
		}
		
	}
	
	public int getPlayerAliveStatus(Player player)
	{
		return isAlive.get(player.getUniqueId());
	}
	
	public HashMap<UUID, Integer> getIsAlive() { return isAlive;}
	
	// KILLS STATUS
	public void updateKills(Player player)
	{
		int updateKill = kills.get(player.getUniqueId()) + 1;
		kills.replace(player.getUniqueId(), updateKill);

		// ANOUNCER
		if(updateKill == 3)
		{
			sendMessage(ChatColor.RED + "" + ChatColor.BOLD + player.getName() + ChatColor.GREEN + " is on a " + ChatColor.GREEN +"" + ChatColor.BOLD + "KILLING SPREE!");
		}else if(updateKill == 4)
		{
			sendMessage(ChatColor.RED + "" + ChatColor.BOLD + player.getName() + ChatColor.GREEN + " is " + ChatColor.GREEN +"" + ChatColor.BOLD + "DOMINATING!");
		}else if(updateKill == 5)
		{
			sendMessage(ChatColor.RED + "" + ChatColor.BOLD + player.getName() + ChatColor.GREEN +"" + ChatColor.BOLD + "SHOULD BE IN JAIL!");
		}else if(updateKill == 6) 
		{
			sendMessage(ChatColor.GREEN + "Someone please " + ChatColor.DARK_GREEN + "" + ChatColor.BOLD + " STOP " + ChatColor.RED + "" + ChatColor.BOLD + player.getName());
		}else if(updateKill >= 7)
		{
			sendMessage(ChatColor.RED + "" + ChatColor.BOLD + player.getName() + ChatColor.GREEN + " is " + ChatColor.GREEN +"" + ChatColor.BOLD + "GODLIKE!");
		}
	}
	
	
	public HashMap<UUID, Integer> getKills(){return kills;}
	
	// KITS
	public void updateKits(Player player, String kit)
	{
		kits.replace(player.getUniqueId(), kit);
	}
	
	public String getKitName(Player player)
	{
		return kits.get(player.getUniqueId()); 
	}
	
	public HashMap<UUID, String> getKits(){return kits;}
	
	// SCOREBOARD
	
	public void CreateBoard(Player player)
	{
		/* Scoreboard
		 * 
		 *  Game Status
		 *  - 7
		 *  GameState = 6
		 *  Players Left = 5
		 *  Kills = 4 
		 *  - 3
		 *  WEBSITE 2
		 *  URL 1
		 */
		
		Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		
		String currentState = new String();
		if(state== GameState.RECRUITING || state == GameState.COUNTDOWN)
		{
			currentState = "Recruiting";
		}
		else if(state ==GameState.PRELIVE)
		{
			currentState = "Invincibile";
		}
		else if(state == GameState.LIVE)
		{
			currentState = "Surviving";
		}else if(state == GameState.ENDING)
		{
			currentState = "Ending";
		}
		
		Objective obj = board.registerNewObjective("test", "dummy", "yo");
		obj.setDisplayName(ChatColor.GREEN.toString() + ChatColor.BOLD + ">> GAME STATUS <<");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
	
		Score blankline1 = obj.getScore(" ");
		blankline1.setScore(5);
		
		// State
		Team gameState = board.registerNewTeam("game-state");
		gameState.addEntry(ChatColor.DARK_GRAY.toString());
		gameState.setPrefix(ChatColor.GREEN.toString() + ChatColor.BOLD + "Stage:");
		gameState.setSuffix(ChatColor.AQUA.toString() + ChatColor.BOLD + currentState);
		obj.getScore(ChatColor.DARK_GRAY.toString()).setScore(4);
		
		// Players left
		Team playersLeft = board.registerNewTeam("players-left");
		playersLeft.addEntry(ChatColor.DARK_RED.toString());
		playersLeft.setPrefix(ChatColor.GREEN.toString() + ChatColor.BOLD + "Players:");
		playersLeft.setSuffix(ChatColor.AQUA.toString() + ChatColor.BOLD + getPlayersAlive());
		obj.getScore(ChatColor.DARK_RED.toString()).setScore(3);
		
		// Player kills
		Team playerKills = board.registerNewTeam("kills");
		playerKills.addEntry(ChatColor.DARK_BLUE.toString());
		playerKills.setPrefix(ChatColor.GREEN.toString() + ChatColor.BOLD +"Kills:");
		playerKills.setSuffix(ChatColor.AQUA.toString() + ChatColor.BOLD + kills.get(player.getUniqueId()));
		obj.getScore(ChatColor.DARK_BLUE.toString()).setScore(2);
		
		Score blankline2 = obj.getScore("  ");
		blankline2.setScore(1);
		
		player.setScoreboard(board);
		
	}
	public void updateScoreboardState()
	{
		String currentState = new String();
		if(state== GameState.RECRUITING || state == GameState.COUNTDOWN)
		{
			currentState = "Recruiting";
		}
		else if(state ==GameState.PRELIVE)
		{
			currentState = "Invincibile";
		}
		else if(state == GameState.LIVE)
		{
			currentState = "Surviving";
		}else if(state == GameState.ENDING)
		{
			currentState = "Ending";
		}
		for(UUID uuid: players)
		{
			Bukkit.getPlayer(uuid).getScoreboard().getTeam("game-state").setSuffix(ChatColor.AQUA.toString() + ChatColor.BOLD + currentState);
		}
	}
	
	public void updateScoreboardPlayers()
	{
		for(UUID uuid: players)
		{
			Bukkit.getPlayer(uuid).getScoreboard().getTeam("players-left").setSuffix(ChatColor.AQUA.toString() + ChatColor.BOLD + getPlayersAlive());
			if(state == GameState.LIVE || state == GameState.PRELIVE)
			{
				updateSign(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "=>ARENA " + id + "<=", ChatColor.GREEN + "ONGOING", ChatColor.BLACK.toString() + getPlayersAlive() + "/20", ChatColor.RED + "Can't Join");
			}
		}
	}
	
	public void updateScoreboardKills(Player player)
	{
		
		player.getScoreboard().getTeam("kills").setSuffix(ChatColor.AQUA.toString() + ChatColor.BOLD + kills.get(player.getUniqueId()));
	}
	
	// UPDATE SIGN
	public void updateSign(String line1, String line2, String line3, String line4)
	{
		try {
			Sign wallSign = (Sign) sign.getBlock().getState();
			wallSign.setLine(0, line1 );
			wallSign.setLine(1, line2);
			wallSign.setLine(2, line3);
			wallSign.setLine(3, line4);
			wallSign.update();
		}
		catch(Exception e)
		{
			
		}
		
		
	}

	// UPDATE BORDER
	public void updateBorder()
	{	
		if(borderSize >=40)
		{
			borderSize -= borderSubtract;
			border.setSize(borderSize);
			border.setCenter(spawn.getX(), spawn.getZ());
		}
		else
		{
			border.setSize(40);
			border.setCenter(spawn.getX(), spawn.getZ());
			game.setIsFeastState(false);
		}
		
	}
	
	// COOLDOWNS
	public long getPlayerCooldown(Player player)
	{
		return cooldowns.get(player.getUniqueId());
	}
	
	public void setCooldown(Player player, long cooldown)
	{
		cooldowns.replace(player.getUniqueId(), cooldown);
	}
	
	// DELETE FOLDER
	public void deleteFolder(File folder) {
		File[] files = folder.listFiles();
		if(files != null) {
			for(File file : files) {
				if(file.isDirectory()) {
					deleteFolder(file);
				} else {
					file.delete();
				}
			}
		}
		folder.delete();
	}
	
	// OTHER
	public int getID() {return id;}
	public List<UUID> getPlayers(){return players;}
	public GameState getState() {return state;}
	public Game getGame() { return game;}
	public void newCountdown(){	countdown = new Countdown(this);}
	public Location getSpawn() { return spawn;}
	public void setState(GameState  state) { this.state = state;}
	public boolean canJoin() { return canJoin;}
	public void setJoinState(boolean state) { this.canJoin = state;}
}




