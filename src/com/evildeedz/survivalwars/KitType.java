package com.evildeedz.survivalwars;

import org.bukkit.ChatColor;
import org.bukkit.Material;

public enum KitType {
	
	BUTCHER(ChatColor.GOLD.toString() + ChatColor.BOLD + "Butcher", Material.DIAMOND_SWORD, new String[] {ChatColor.WHITE + "Permanent Strength I Effect!"}, "butcher"),
	FLASH(ChatColor.RED.toString() + ChatColor.BOLD + "Flash", Material.REDSTONE_TORCH, new String[] {ChatColor.WHITE + "Permanent Swiftness II Effect!"}, "flash"),
	FROST(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "Frost", Material.ICE , new String[] {ChatColor.WHITE + "20% Chance to Slow Your Opponent for 3 Seconds on Every Hit!"}, "frost"),
	WIZARD(ChatColor.YELLOW.toString() + ChatColor.BOLD + "Wizard", Material.FIRE_CHARGE , new String[] {ChatColor.WHITE + "Have the ability to throw a Fireball!", " ", ChatColor.GOLD + "Cooldown - 200 Seconds"}, "wizard"),
	BLUR(ChatColor.BLUE.toString() + ChatColor.BOLD + "Blur", Material.GLASS_PANE, new String[] {ChatColor.WHITE + "Turns you Invisible for 8 Seconds!", " ", ChatColor.GOLD + "Cooldown - 60 Seconds"}, "blur"),
	VIKING(ChatColor.DARK_BLUE.toString() + ChatColor.BOLD + "Viking", Material.IRON_AXE, new String[] {ChatColor.WHITE + "Spawn with an Stone Axe with SHARPNESS II & Unbreaking X!"}, "viking"),
	BLUBBER(ChatColor.DARK_PURPLE.toString() + ChatColor.BOLD + "Blubber", Material.ROTTEN_FLESH, new String[] {ChatColor.WHITE + "Restore Hunger by 10 Points (5 Food levels)!", " ", ChatColor.GOLD + "Cooldown - 30 Seconds"}, "blubber"),
	KINGOFCREEPERS(ChatColor.GOLD.toString() + ChatColor.BOLD + "King Of Creepers", Material.CREEPER_SPAWN_EGG, new String[] {ChatColor.WHITE + "Launches a Horde of Creepers at your Location!", ChatColor.WHITE + "Creeper's also never target you!", ChatColor.WHITE + "You are Impervious to Blast Damage.", " ", ChatColor.GOLD + "Cooldown - 300 Seconds"}, "kingofcreepers"),
	BLINK(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "Blink", Material.ENDER_PEARL, new String[] {ChatColor.WHITE + "Teleports you to any Block that you point at!", " ", ChatColor.GOLD + "Cooldown - 35 Seconds"}, "blink"),
	TOSS(ChatColor.GRAY.toString() + ChatColor.BOLD + "Toss", Material.STONE, new String[] {ChatColor.WHITE + "Launch a Horde of players in a 5 block radius into the sky!", ChatColor.WHITE + "Players take damage on landing.", " ", ChatColor.GOLD + "Cooldown - 100 Seconds"}, "toss"),
	ELEMENTAL(ChatColor.GOLD.toString() + ChatColor.BOLD + "Elemental", Material.ORANGE_DYE, new String[] {ChatColor.WHITE + "You start with 3 Orbs with different Abilities", " ", ChatColor.BLUE.toString() + ChatColor.BOLD + "Harmony Orb " + ChatColor.WHITE + "provides Health Regen I", ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "Swift Orb " + ChatColor.WHITE + "provides Swiftness II", ChatColor.RED.toString() + ChatColor.BOLD + "Overwhelming Orb " + ChatColor.WHITE + "provides Strength I" , " ", ChatColor.WHITE + "Only One Orb can stay activated at a time."}, "elemental"),
	CURSEDEDGE(ChatColor.RED.toString() + ChatColor.BOLD + "Cursed Blade", Material.NETHERITE_SWORD, new String[] {ChatColor.WHITE + "Your Sword Attacks have a 30% Chance to", ChatColor.WHITE + "give enemy a Wither Effect for 5 seconds!"}, "cursededge"),
	BEASTMASTER(ChatColor.YELLOW.toString() + ChatColor.BOLD + "Beastmaster", Material.WOLF_SPAWN_EGG, new String[] {ChatColor.WHITE + "Spawn 3 wolves at your location to guard you!", " ", ChatColor.GOLD + "Cooldown - 240 Seconds"}, "beastmaster"),
	NOVABURST(ChatColor.BLUE.toString() + ChatColor.BOLD + "Nova Burst", Material.BLUE_ICE, new String[] {ChatColor.WHITE + "In an ice burst, gives players in a 5 block", ChatColor.WHITE + "radius near you EXTREME Slowness for 6 Seconds!", " ", ChatColor.GOLD + "Cooldown - 60 Seconds"}, "novaburst"),
	PEGASUS(ChatColor.GRAY.toString() + ChatColor.BOLD + "Pegasus", Material.FEATHER, new String[] {ChatColor.WHITE + "Gives user ability to fly for 7 Seconds!", ChatColor.WHITE + "Player WILL take fall damage on duration end!", " ", ChatColor.GOLD + "Cooldown - 40 Seconds"}, "pegasus"),
	FROSTARROWS(ChatColor.DARK_BLUE.toString() + ChatColor.BOLD + "Frost Arrows", Material.SPECTRAL_ARROW, new String[] {ChatColor.WHITE + "You Spawn with a Bow and 64 Spectral Arrows", ChatColor.WHITE + "Your arrows Slow enemy on Hit for 3 Seconds!", ChatColor.WHITE + "Your Bow has Unbreaking X & Power II"}, "frostarrows"),
	BERSERK(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "Berserk", Material.GOLDEN_APPLE, new String[] {ChatColor.WHITE + "Gives user Strength II for 10 Seconds", " ", ChatColor.GOLD + "Cooldown - 60 Seconds"}, "berserk"),
	SMOKEOFDECEIT(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "Smoke Of Deceit", Material.ENDER_EYE, new String[] {ChatColor.WHITE + "Players in a 10 Block radius are given", ChatColor.WHITE + "blindness for 8 Seconds", " ", ChatColor.GOLD + "Cooldown - 120 Seconds"}, "smokeofdeceit");
	
	private String display;
	private Material material;
	private String[] description;
	private String kitName;
	
	private KitType(String display, Material material, String[] description, String kitName)
	{	
		this.display = display;
		this.material = material;
		this.description = description;
		this.kitName = kitName;
		
	}

	public String getDisplay() { return display; }
	public Material getMaterial() { return material; }
	public String[] getDescription() { return description; }
	public String getKitName() { return kitName; }
}
