package com.evildeedz.survivalwars;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import com.evildeedz.survivalwars.Commands.ArenaCommand;
import com.evildeedz.survivalwars.Commands.BalCommand;
import com.evildeedz.survivalwars.Commands.GiveCommand;
import com.evildeedz.survivalwars.Commands.LeaveCommand;
import com.evildeedz.survivalwars.Commands.WhisperCommand;
import com.evildeedz.survivalwars.GameLogic.ArenaSigns;
import com.evildeedz.survivalwars.GameLogic.EntityDamage;
import com.evildeedz.survivalwars.GameLogic.GameListener;
import com.evildeedz.survivalwars.GameLogic.KitSelector;
import com.evildeedz.survivalwars.GameLogic.Leaderboards;
import com.evildeedz.survivalwars.GameLogic.LiveCompass;
import com.evildeedz.survivalwars.GameLogic.SpectatorCompass;
import com.evildeedz.survivalwars.GameLogic.Stews;
import com.evildeedz.survivalwars.Kits.Beastmaster;
import com.evildeedz.survivalwars.Kits.Berserk;
import com.evildeedz.survivalwars.Kits.Blink;
import com.evildeedz.survivalwars.Kits.Blubber;
import com.evildeedz.survivalwars.Kits.Blur;
import com.evildeedz.survivalwars.Kits.CursedEdge;
import com.evildeedz.survivalwars.Kits.Elemental;
import com.evildeedz.survivalwars.Kits.Frost;
import com.evildeedz.survivalwars.Kits.FrostArrows;
import com.evildeedz.survivalwars.Kits.KingOfCreepers;
import com.evildeedz.survivalwars.Kits.Novaburst;
import com.evildeedz.survivalwars.Kits.Pegasus;
import com.evildeedz.survivalwars.Kits.SmokeOfDeceit;
import com.evildeedz.survivalwars.Kits.Toss;
import com.evildeedz.survivalwars.Kits.Wizard;


/* TODO
 * 	
 * Spectator cant come near
 * Leaderboards
 * Admin Commands + Stats Commands
 * Online Command Setup
 * Mini Feast (check dc)
 * custom spawn at prelive start
 */

public class Main extends JavaPlugin{

	private static Main instance;
	
	@Override
	public void onEnable()
	{
		Main.instance = this;
		System.out.println("Plugin started");
		
		new Config(this);
		new playerDataYML(this);
		new Manager();
		new Leaderboards();
		
		// Commands
		getCommand("arena").setExecutor(new ArenaCommand());
		getCommand("leave").setExecutor(new LeaveCommand());
		getCommand("give").setExecutor(new GiveCommand());
		getCommand("bal").setExecutor(new BalCommand());
		getCommand("balance").setExecutor(new BalCommand());
		getCommand("money").setExecutor(new BalCommand());
		getCommand("msg").setExecutor(new WhisperCommand());
		getCommand("w").setExecutor(new WhisperCommand());
		getCommand("whisper").setExecutor(new WhisperCommand());
		getCommand("r").setExecutor(new WhisperCommand());
		
		// Game Logic
		Bukkit.getPluginManager().registerEvents(new GameListener(), this);
		Bukkit.getPluginManager().registerEvents(new LiveCompass(), this);
		Bukkit.getPluginManager().registerEvents(new ArenaSigns(), this);
		Bukkit.getPluginManager().registerEvents(new Stews(), this);
		Bukkit.getPluginManager().registerEvents(new KitSelector(), this);
		Bukkit.getPluginManager().registerEvents(new EntityDamage(), this);
		Bukkit.getPluginManager().registerEvents(new SpectatorCompass(), this);
		
		// Kits
		Bukkit.getPluginManager().registerEvents(new Frost(), this);
		Bukkit.getPluginManager().registerEvents(new Wizard(), this);
		Bukkit.getPluginManager().registerEvents(new Blur(), this);
		Bukkit.getPluginManager().registerEvents(new Blubber(), this);
		Bukkit.getPluginManager().registerEvents(new KingOfCreepers(), this);
		Bukkit.getPluginManager().registerEvents(new CursedEdge(), this);
		Bukkit.getPluginManager().registerEvents(new FrostArrows(), this);
		Bukkit.getPluginManager().registerEvents(new Blink(), this);
		Bukkit.getPluginManager().registerEvents(new Toss(), this);
		Bukkit.getPluginManager().registerEvents(new Beastmaster(), this);
		Bukkit.getPluginManager().registerEvents(new Elemental(), this);
		Bukkit.getPluginManager().registerEvents(new Novaburst(), this);
		Bukkit.getPluginManager().registerEvents(new Berserk(), this);
		Bukkit.getPluginManager().registerEvents(new SmokeOfDeceit(), this);
		Bukkit.getPluginManager().registerEvents(new Pegasus(), this);
		loadRecipe();
	}
	
	public static Main getInstance() {return instance;}
	
	// STEW RECIPE
	public void loadRecipe()
	{
		/*
		 * ^ = Cactus
		 * % = Bowl
		 * 
		 * ^ = Cocoa Bean
		 * % = Bowl
		 */
		
		ItemStack cactusStew = new ItemStack(Material.MUSHROOM_STEW);
		ItemMeta cactusStewMeta = cactusStew.getItemMeta();
		cactusStewMeta.setDisplayName("Cactus Stew");
		cactusStew.setItemMeta(cactusStewMeta);
		NamespacedKey nsKey1 = new NamespacedKey(this, "cactus_stew");
		ShapedRecipe cactusRecipe = new ShapedRecipe(nsKey1,cactusStew);
		cactusRecipe.shape("^", "%");
		
		cactusRecipe.setIngredient('^', Material.CACTUS);
		cactusRecipe.setIngredient('%', Material.BOWL);
		
		this.getServer().addRecipe(cactusRecipe);
		
		
		ItemStack cocoaStew = new ItemStack(Material.MUSHROOM_STEW);
		ItemMeta cocoaStewMeta = cocoaStew.getItemMeta();
		cocoaStewMeta.setDisplayName("Cocoa Stew");
		cocoaStew.setItemMeta(cocoaStewMeta);
		NamespacedKey nsKey2 = new NamespacedKey(this, "cocoa_stew");
		ShapedRecipe cocoaRecipe = new ShapedRecipe(nsKey2,cocoaStew);
		cocoaRecipe.shape("^", "%");
		
		cocoaRecipe.setIngredient('^', Material.COCOA_BEANS);
		cocoaRecipe.setIngredient('%', Material.BOWL);
		
		this.getServer().addRecipe(cocoaRecipe);
	}
	

}