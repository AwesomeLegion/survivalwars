package com.evildeedz.survivalwars;

import java.util.Random;
import java.util.UUID;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class Game extends BukkitRunnable {
	
	private Arena arena;
	private KitManager kitManager;
	
	private int preLiveSeconds;
	private int liveSeconds;
	private int endingSeconds;
	private boolean isFeast;
	
	public Game(Arena arena)
	{
		this.arena = arena;
		this.preLiveSeconds = Config.getPreliveSeconds();
		this.liveSeconds = Config.getLiveSeconds();
		this.endingSeconds = Config.getEndingSeconds();
		isFeast = false;
	}
	
	public void start()
	{
		kitManager = new KitManager();
		arena.setState(GameState.PRELIVE);
		arena.updateScoreboardState();
		Bukkit.getWorld(Config.getWorldFromArenaID(arena.getID())).setTime(6000);
		arena.sendMessage(ChatColor.GREEN + "Survival Begins, you have invulnerability, gather weapons for survival before it wears off!");
		this.runTaskTimer(Main.getInstance(), 0, 20);
		arena.playSound(Sound.ENTITY_WITHER_SPAWN);
		// giving out kits to every player
		for(Entry<UUID, String> kit: arena.getKits().entrySet())
		{
			String kitName = kit.getValue();
			if(kitName == "none")
			{
				String[] ownedKits = playerDataYML.getPlayerKits(kit.getKey());
				Random rand = new Random();
				int r = rand.nextInt(ownedKits.length);
				kitName = ownedKits[r];
				arena.updateKits(Bukkit.getPlayer(kit.getKey()), kitName);
				Bukkit.getPlayer(kit.getKey()).sendMessage(ChatColor.GREEN + "You have been given a Random Kit " + ChatColor.YELLOW + kitName);
			}
			kitManager.giveKits(Bukkit.getPlayer(kit.getKey()), kitName);
			
		}
		
		// giving compass to each player
		for(UUID uuid: arena.getPlayers())
		{
			ItemStack compass = new ItemStack(Material.COMPASS);
			Bukkit.getPlayer(uuid).getInventory().addItem(compass);
		}
		
		arena.updateSign(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "=>ARENA " + arena.getID() + "<=", ChatColor.GREEN + "ONGOING", ChatColor.BLACK.toString() + arena.getPlayersAlive() + "/20", ChatColor.RED + "Can't Join");

		// Teleporting all players to Arena Spawn
		for(UUID uuid : arena.getPlayers())
		{
			Bukkit.getPlayer(uuid).teleport(arena.getSpawn());
		}
	}
	
	// Updating Scoreboard, Controlling prelive and live state
	@Override
	public void run()
	{
		/* GAME MAP 
		 * 
		 * PRE LIVE = 12 pm DAY 1 (6000 ticks)
		 * LIVE = 6 pm DAY 1 (12000 ticks) (0 ticks total live)
		 * FEAST = 4 am Day 3 (22000 ticks) (34000 ticks total live) (1700 seconds)
		 * GAME END = 6pm pm DAY 3 (12000 ticks) (48000 ticks total live) 
		 * Total Live Gameplay = 40 mins (2400 seconds)
		 * Anounce Feast begins at 1000 sec left, feast at 700 sec left, game ending anounce at 600 sec, game end at 0 sec
		 */
		

		if(arena.getState() == GameState.PRELIVE)
		{
			if(preLiveSeconds == 0)
			{
				
				arena.setState(GameState.LIVE);
				arena.updateScoreboardState();
				arena.sendMessage(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "Game has commenced! Last one to survive wins!");
			}else if(preLiveSeconds == 300)
			{
				arena.sendMessage(ChatColor.RED + "Invulnerability wears off in 5 minutes.");
				
			}else if(preLiveSeconds == 240)
			{
				arena.sendMessage(ChatColor.RED + "Invulnerability wears off in 4 minutes.");
				
			}else if(preLiveSeconds == 180)
			{
				arena.sendMessage(ChatColor.RED + "Invulnerability wears off in 3 minutes.");
				
			}else if(preLiveSeconds == 120)
			{
				arena.sendMessage(ChatColor.RED + "Invulnerability wears off in 2 minutes.");
				
			}else if(preLiveSeconds == 60)
			{
				arena.sendMessage(ChatColor.RED + "Invulnerability wears off in 1 minute.");
			}
			else  if(preLiveSeconds <= 5 && preLiveSeconds >=1 || preLiveSeconds == 45 || preLiveSeconds == 30 || preLiveSeconds == 15 || preLiveSeconds == 10)
			{
				arena.sendMessage(ChatColor.RED + "Invulnerability wears off in " + preLiveSeconds + " seconds.");
			}
			
			
			preLiveSeconds--;
		}else if(arena.getState() == GameState.LIVE)
		{
			// Game lasts max 40 mins
			if(liveSeconds == 1000)
			{
				// Feast begins in 5 mins
				arena.sendMessage(ChatColor.GREEN + "Feast Begins in 5 minutes!");
			}
			else if(liveSeconds == 940)
			{
				arena.sendMessage(ChatColor.GREEN + "Feast Begins in 4 minutes!");
			}
			else if(liveSeconds == 880)
			{
				arena.sendMessage(ChatColor.GREEN + "Feast Begins in 3 minutes!");
			}
			else if(liveSeconds == 820)
			{
				arena.sendMessage(ChatColor.GREEN + "Feast Begins in 2 minutes!");
			}else if(liveSeconds == 760)
			{
				arena.sendMessage(ChatColor.GREEN + "Feast Begins in 1 minute! MAKE SURE your Inventory Has 1 Vacant Slot");
			}
			else if(liveSeconds <= 705 && liveSeconds >=701)
			{
				arena.sendMessage(ChatColor.GREEN + "Feast Begins in " + liveSeconds);
			}
			else if(liveSeconds == 700)
			{
				arena.sendMessage(ChatColor.AQUA.toString() + ChatColor.BOLD+ "FEAST HAS BEGUN! Check your Inventories! Borders Now SHRINKING!");
				isFeast = true;
				arena.playSound(Sound.ENTITY_WITHER_SPAWN);
				// Feast features + sound
				for(UUID uuid : arena.getPlayers())
				{
					Player player = (Player) Bukkit.getPlayer(uuid);
					if(arena.getPlayerAliveStatus(player) == 1)
					{
						ItemStack eChest = new ItemStack(Material.ENDER_CHEST);
						ItemMeta eChestMeta = eChest.getItemMeta();
						eChestMeta.setDisplayName(ChatColor.DARK_BLUE + player.getName() + "'s FEAST");
						eChest.setItemMeta(eChestMeta);
						if(player.getInventory().firstEmpty() != -1)
							player.getInventory().addItem(eChest);
						else
							player.getWorld().dropItem(player.getLocation(), eChest);
					}
				}
			}
			else if(liveSeconds == 600)
			{
				// Game ends in 10 mins
				arena.sendMessage(ChatColor.GREEN + "Game ends in 10 minutes!");
			}
			else if(liveSeconds == 300)
			{
				arena.sendMessage(ChatColor.GREEN + "Game ends in 5 minutes!");
			}
			else if(liveSeconds == 60)
			{
				arena.sendMessage(ChatColor.GREEN + "Game ends in 1 minute!");
			}
			else if(liveSeconds == 30)
			{
				arena.sendMessage(ChatColor.GREEN + "Game ends in 30 seconds!");
			}
			else if(liveSeconds == 10)
			{
				arena.sendMessage(ChatColor.GREEN + "Game ends in 10 seconds!");
			}
			else if(liveSeconds <=5 && liveSeconds >=1)
			{
				arena.sendMessage(ChatColor.GREEN + "Game ends in " + liveSeconds);
			}
			else if(liveSeconds == 0)
			{
				// game ends check highest kills, if 2 players with highest kills then both winners
				arena.playSound(Sound.ENTITY_LIGHTNING_BOLT_THUNDER);
				Ending();
				
			}
			liveSeconds--;
		}
		else if(arena.getState() == GameState.ENDING)
		{
			if(endingSeconds == 0)
			{
				arena.Reset();
				cancel();
				return;
			}
			
			endingSeconds--;
		}
		
		
		// Border Shrink Call
		if(isFeast == true && arena.getState() == GameState.LIVE)
		{
			arena.updateBorder();
		}
	}
	
	//ENDING
	public void Ending()
	{
		arena.setState(GameState.ENDING);
		arena.updateScoreboardState();
		isFeast = false;
		
		// Displaying Winners Name
		if(arena.getPlayersAlive() == 1)
		{
			for(Entry<UUID, Integer> entry: arena.getIsAlive().entrySet())
			{
				if(entry.getValue() == 1)
				{
					arena.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "The Winner of the Battle is " + ChatColor.DARK_RED + "" + ChatColor.BOLD + Bukkit.getPlayer(entry.getKey()).getName() + "(" + arena.getKitName(Bukkit.getPlayer(entry.getKey())) + ") !!!");
					playerDataYML.updatePlayerWins(entry.getKey());
					playerDataYML.updatePlayerBalance(entry.getKey(), 3000);
				}
			}
		}
		else if(arena.getPlayersAlive() > 1)
		{
			String winners = ChatColor.RED.toString() + ChatColor.BOLD + "The Winners of the Battle are ";
			for(Entry<UUID, Integer> entry: arena.getIsAlive().entrySet())
			{
				if(entry.getValue() == 1)
				{
					winners = winners+ ChatColor.DARK_RED + ChatColor.BOLD + Bukkit.getPlayer(entry.getKey()).getName() + "(" + arena.getKitName(Bukkit.getPlayer(entry.getKey())) + ") ";
					playerDataYML.updatePlayerWins(entry.getKey());
					playerDataYML.updatePlayerBalance(entry.getKey(), 3000);
				}
			}
			winners = winners + "!!!";
			arena.sendMessage(winners);
		}
		
		// Removing all entities
		for(Entity e: Bukkit.getWorld(Config.getWorldFromArenaID(arena.getID())).getEntities())
		{
			if(!(e instanceof Player))
			{
				e.remove();
			}
		}
		
		// Sign Update
		arena.updateSign(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "=>ARENA " + arena.getID() + "<=", ChatColor.RED + "ENDING", ChatColor.BLACK.toString() + arena.getPlayersAlive() + "/20", ChatColor.RED + "Can't Join");
	
		
		
	}
	
	

	// OTHER
	public Arena getArena()	{return arena;}
	public void setIsFeastState(boolean state) { this.isFeast = state; }
	public boolean getIsFeastState() {return isFeast;}
}
