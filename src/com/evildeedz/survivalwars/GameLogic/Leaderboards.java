package com.evildeedz.survivalwars.GameLogic;

import java.util.LinkedHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.scheduler.BukkitRunnable;

import com.evildeedz.survivalwars.Config;
import com.evildeedz.survivalwars.Main;
import com.evildeedz.survivalwars.playerDataYML;

import org.bukkit.ChatColor;

public class Leaderboards extends BukkitRunnable{

	private Location killBoard;
	private Location winBoard;
	private ArmorStand killStand;
	private ArmorStand winStand;
	
	public Leaderboards()
	{
		
		this.runTaskTimer(Main.getInstance(), 0, 1200);
	}
	
	@Override
	public void run()
	{
		int counter = 0;
		killBoard = Config.leaderboardKills();
		winBoard = Config.leaderboardWins();
		// Getting Arrays from HashMap For KILLS
		LinkedHashMap <String,Integer> killMap = playerDataYML.getTopKills();
		String[] killMapString = new String[10];
		int[] killMapInt = new int[10];
		
		for(String str: killMap.keySet())
		{
			if(counter <10)
			{
				killMapString[counter++] = str;
			}
		}
		
		counter = 0;
		
		for(int in: killMap.values())
		{
			if(counter <10)
			{
				killMapInt[counter++] = in;
			}
		}
		
		// Getting Arrays from HashMap For WINS
		counter = 0;
		LinkedHashMap <String,Integer> winMap = playerDataYML.getTopWins();
		String[] winMapString = new String[10];
		int[] winMapInt = new int[10];
		
		for(String str: winMap.keySet())
		{
			if(counter <10)
			{
				winMapString[counter++] = str;
			}
		}
		counter = 0;
		
		for(int in: winMap.values())
		{
			if(counter <10)
			{
				winMapInt[counter++] = in;
			}
		}
		
		// DESTROYING CURRENT STANDS
		for(Entity entity: Bukkit.getWorld(Config.getLobbyWorld()).getEntities())
		{
			if(entity instanceof ArmorStand)
			{
				entity.remove();
			}
		}
		
		// KILL STAND
		for(int i = 0;i<11;i++)
		{
			killBoard = killBoard.add(0,0.4,0);
			killStand = (ArmorStand) Bukkit.getWorld(Config.getLobbyWorld()).spawnEntity(killBoard, EntityType.ARMOR_STAND);
			killStand.setVisible(false);
			killStand.setGravity(false);
			killStand.setCustomName(" ");
			if(i == 10)
				killStand.setCustomName(ChatColor.YELLOW.toString() + ChatColor.BOLD.toString() + ChatColor.MAGIC.toString() + "1" + ChatColor.DARK_BLUE.toString() + ChatColor.BOLD + " Top Kills " + ChatColor.YELLOW.toString() + ChatColor.BOLD.toString() + ChatColor.MAGIC.toString() + "1");
			else
			{
				if(killMapString[9-i]!= null && killMapInt[9-i]>=0)
				killStand.setCustomName(ChatColor.GRAY + "[" + (10-i) + "]" + ChatColor.GREEN + " " + killMapString[9-i] + ChatColor.GRAY + " - " + ChatColor.GREEN + killMapInt[9-i]);	
			}
			killStand.setCustomNameVisible(true);
		}

		// WIN STAND
		for(int i = 0;i<11;i++)
		{
			winBoard = winBoard.add(0,0.4,0);
			winStand = (ArmorStand) Bukkit.getWorld(Config.getLobbyWorld()).spawnEntity(winBoard, EntityType.ARMOR_STAND);
			winStand.setVisible(false);
			winStand.setGravity(false);
			winStand.setCustomName(" ");
			if(i == 10)
				winStand.setCustomName(ChatColor.YELLOW.toString() + ChatColor.BOLD.toString() + ChatColor.MAGIC.toString() + "1" + ChatColor.DARK_BLUE.toString() + ChatColor.BOLD + " Top Wins " + ChatColor.YELLOW.toString() + ChatColor.BOLD.toString() + ChatColor.MAGIC.toString() + "1");
			else	
			{
				if(winMapString[9-i] != null && winMapInt[9-i] >=0)
				{
					winStand.setCustomName(ChatColor.GRAY + "[" + (10-i) + "]" + ChatColor.GREEN + " " + winMapString[9-i] + ChatColor.GRAY + " - " + ChatColor.GREEN + winMapInt[9-i]);	
					
				}
				
			}
			winStand.setCustomNameVisible(true);
		}
	}
}
