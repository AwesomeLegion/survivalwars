package com.evildeedz.survivalwars.GameLogic;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.KitType;
import com.evildeedz.survivalwars.Manager;
import com.evildeedz.survivalwars.playerDataYML;

public class KitSelector implements Listener{
	
	private Inventory gui;
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getAction().equals(Action.RIGHT_CLICK_AIR))
		{
			if(e.getPlayer().getInventory().getItemInMainHand() != null || e.getPlayer().getInventory().getItemInOffHand() != null)
			{	
				// Nether Star for Kit Selection
				if(e.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.NETHER_STAR))
				{
					if(Manager.isPlaying(player))
					{
						if(Manager.getArena(player).getState() == GameState.COUNTDOWN || Manager.getArena(player).getState() == GameState.RECRUITING)
						{
							kitsGUI(player);
						}
					}
				}
			}
		}				
	}
	
	// INVENTORY CLICK EVENT
	@EventHandler
	public void onClicK(InventoryClickEvent e)
	{
		
		Player player = (Player) e.getWhoClicked();
		if(Manager.isPlaying(player))
		{
			if(Manager.getArena(player).getState() == GameState.COUNTDOWN || Manager.getArena(player).getState() == GameState.RECRUITING)
			{
				if(e.getInventory().equals(gui))
				{
					// Iterating through kit types
					for(KitType type: KitType.values())
					{
						// Making Sure Kit type isnt null
						if(type != null)
						{	
							// If material clicked equals the material found while iterating
							if(e.getCurrentItem().getType() != null && e.getCurrentItem().getType() == type.getMaterial() )
							{
								// What to do if left click
								if(e.isLeftClick())
								{
									// If player owns kit
									if(playerDataYML.doesPlayerOwnKit(player.getUniqueId(), type.getKitName()))
									{
										// If kit already equipped
										if(!Manager.getArena(player).getKitName(player).equals(type.getKitName()))
										{
											player.sendMessage(ChatColor.GREEN + "Equipped Kit " + type.getDisplay());
											Manager.getArena(player).updateKits(player, type.getKitName());
											player.closeInventory();
										}else
										{
											player.sendMessage(ChatColor.RED + "You have already equipped this kit!");	
											player.closeInventory();
										}
									}
									else
									{
										player.sendMessage(ChatColor.RED + "You do not own this kit, Right Click to Buy for $7000");
									}
								}
								
								// What to do if Right click
								if(e.isRightClick())
								{
									if(!playerDataYML.doesPlayerOwnKit(player.getUniqueId(), type.getKitName()))
									{
										if(playerDataYML.getPlayerBalance(player.getUniqueId()) >= 7000)
										{
											playerDataYML.updatePlayerBalance(player.getUniqueId(), -7000);
											playerDataYML.addKit(player.getUniqueId(), type.getKitName());
											player.closeInventory();
											player.sendMessage(ChatColor.GREEN+ "Successfully purchased kit " + type.getDisplay());
										}else
										{
											player.sendMessage("You don't have enough money!");
											player.closeInventory();
										}
									}
									
								}
							}
							
						}
					}
				}
				e.setCancelled(true);
			}
		}
		
	}
	
	public void kitsGUI(Player player)
	{
		gui = Bukkit.createInventory(player, 36,ChatColor.BLUE + "Kit Selection");
		for(KitType type: KitType.values())
		{
			ItemStack is = new ItemStack(type.getMaterial());
			ItemMeta isMeta = is.getItemMeta();
			isMeta.setDisplayName(type.getDisplay());
			if(playerDataYML.doesPlayerOwnKit(player.getUniqueId(), type.getKitName()))
			{	
				String[] loreStr = new String[type.getDescription().length + 2];
				for(int i = 0; i < type.getDescription().length; i++)
				{
					loreStr[i] = type.getDescription()[i];
				}
				loreStr[type.getDescription().length] = " ";
				loreStr[type.getDescription().length+1] = ChatColor.GREEN + "Left-Click the Kit to Equip!";
				List<String> lore = Arrays.asList(loreStr);
				isMeta.setLore(lore);
				is.setItemMeta(isMeta);
			}
			else
			{	
				String[] loreStr = new String[type.getDescription().length + 2];
				for(int i = 0; i < type.getDescription().length; i++)
				{
					loreStr[i] = type.getDescription()[i];
				}
				loreStr[type.getDescription().length] = " ";
				loreStr[type.getDescription().length+1] = ChatColor.RED + "Right Click to Buy the Kit for $7000!";
				List<String> lore = Arrays.asList(loreStr);
				isMeta.setLore(lore);
				is.setItemMeta(isMeta);
				is.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
			}
			
			gui.addItem(is);
		}
		
		player.openInventory(gui);
		
	}
}
