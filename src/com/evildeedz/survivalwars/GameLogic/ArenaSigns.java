package com.evildeedz.survivalwars.GameLogic;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import com.evildeedz.survivalwars.Config;
import com.evildeedz.survivalwars.Manager;

public class ArenaSigns implements Listener{
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		if(e.hasBlock() && e.getClickedBlock().getType().equals(Material.ACACIA_WALL_SIGN))
		{
			if(player.hasPermission("group.owner") || player.hasPermission("group.coowner"))
				player.sendMessage(e.getClickedBlock().getLocation() + "");
			
			int id = Config.isArenaSign(e.getClickedBlock().getLocation());
			if(id != -1)
			{		
				if(Manager.isJoinable(id))
				{
					if(!Manager.isPlaying(player))
					{
						if(Manager.getArena(id).canJoin())
						{
							Manager.getArena(id).addPlayer(player);
							
						}else
						{
							player.sendMessage(ChatColor.RED + "You cannot join this game right now!");
						}
					}else
					{
						player.sendMessage(ChatColor.RED + "You are already in a game! To leave type /arena leave");
					}
					
				}else {
					player.sendMessage(ChatColor.RED + "You cannot join this game right now!");
				}
					
				e.setCancelled(true);
			}
		}
	}
}
