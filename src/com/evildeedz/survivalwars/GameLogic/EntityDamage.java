package com.evildeedz.survivalwars.GameLogic;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.Main;
import com.evildeedz.survivalwars.Manager;
import com.evildeedz.survivalwars.playerDataYML;

public class EntityDamage implements Listener{
	
	// ENTITY HITS ENTITY
	@EventHandler
	public void onHit(EntityDamageByEntityEvent e) {
		
		// Checking on the Projectile Damage
		if(e.getDamager() instanceof Projectile && e.getEntity() instanceof Player)
		{
			Player player = (Player) e.getEntity();
			Projectile projectile = (Projectile) e.getDamager();
			
			// If projectile shot by a player
			if(projectile.getShooter() instanceof Player)
			{
				Player damager = (Player) projectile.getShooter();
				
				// If GameState is LIVE & Reciever is ALIVE
				if(Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getPlayerAliveStatus(player) == 1)
				{
					// If player dies on the shot
					if(e.getDamage() >= player.getHealth())
					{						
							e.setCancelled(true);
							killPlayer(player, damager);
					}
				}else
				{
					e.setCancelled(true);
				}
			}
		}
		
		// If Damager is Player
		if(e.getDamager() instanceof Player)
		{
			Player damager = (Player) e.getDamager();
			
			// Are the damager in a game
			if(Manager.isPlaying(damager))
			{	
				// Entity taking damage is a player
				if(e.getEntity() instanceof Player)
				{	
					// If the Damager is Alive
					if(Manager.getArena(damager).getPlayerAliveStatus(damager) == 1)
					{
						
						Player player = (Player) e.getEntity();
						// Spectator Protection || Games are in these States
						if(Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getPlayerAliveStatus(player) == 1)
						{
							// check if the damager kills him
							if(e.getDamage() >= player.getHealth())
							{
								e.setCancelled(true);
								killPlayer(player, damager);
							}
						}
						else
						{
							e.setCancelled(true);
						}
					}else
					{
						// if its ELSE that means the damager isn't alive
						e.setCancelled(true);					
					}
				}
				else
				{
					// Entity taking damage is a Mob and when to cancel it
					if(Manager.getArena(damager).getPlayerAliveStatus(damager) == 0 || Manager.getArena(damager).getState() == GameState.COUNTDOWN || Manager.getArena(damager).getState() == GameState.RECRUITING || Manager.getArena(damager).getState() == GameState.ENDING)
					{
						e.setCancelled(true);
					}
				}
			}
		}	
	}
	
	// ENTITY TAKES DAMAGE
	@EventHandler
	public void onHit(EntityDamageEvent e) {
		
		if(e.getEntity() instanceof Player)
		{
			Player player = (Player) e.getEntity();

			
			if(Manager.isPlaying(player))
			{
				// When to cancel damage
				if(Manager.getArena(player).getState() == GameState.COUNTDOWN || Manager.getArena(player).getState() == GameState.RECRUITING || Manager.getArena(player).getState() == GameState.ENDING || Manager.getArena(player).getState() == GameState.PRELIVE || Manager.getArena(player).getPlayerAliveStatus(player) == 0)
				{
					e.setCancelled(true);
				}
				
				// Making player spectator
				if( Manager.getArena(player).getPlayerAliveStatus(player) == 1 && e.getDamage() >= player.getHealth())
				{ 
					// Checking if damager isnt a human
					if(Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getPlayerAliveStatus(player) == 1)
					{
						try {
							
							EntityDamageByEntityEvent event = (EntityDamageByEntityEvent) e; // Casting events
							if(!(event.getDamager() instanceof Player))
							{	
								// Checking if a PROJECTILE is NOT from a Player instance
								if(event.getDamager() instanceof Projectile)
								{
									Projectile projectile = (Projectile) event.getDamager();
									if(!(projectile.getShooter() instanceof Player))
									{
										e.setCancelled(true);
										setSpectator(player);	
										Manager.getArena(player).sendMessage(ChatColor.DARK_RED + player.getName() + "(" + Manager.getArena(player).getKitName(player) + ")" + ChatColor.RED + " got Shot by them Monsters!");
										Manager.getArena(player).playSound(Sound.ENTITY_LIGHTNING_BOLT_THUNDER);
									}
									
								}
								// Checking if a Creeper Killed Player
								else if(event.getDamager() instanceof Creeper)
								{
									if(!(Manager.getArena(player).getKitName(player).equals("kingofcreepers"))) // We do not add ELSE here because we check that logic in KingOfCreeper class
									{
										if(((Creeper) event.getDamager()).getCustomName() != null)
										{
											String creeperName = ((Creeper) event.getDamager()).getCustomName();
											if(Manager.getArena(player).getPlayers().contains(Bukkit.getPlayer(creeperName).getUniqueId()))
											{
												e.setCancelled(true);
												killPlayer(player, Bukkit.getPlayer(creeperName));
											}
											else
											{
												e.setCancelled(true);
												setSpectator(player);
												Manager.getArena(player).sendMessage(ChatColor.DARK_RED + player.getName() + "(" + Manager.getArena(player).getKitName(player) + ")" + ChatColor.RED + " got Blown up By a Creeper!");
												Manager.getArena(player).playSound(Sound.ENTITY_LIGHTNING_BOLT_THUNDER);
											}
										}
										else
										{												
											e.setCancelled(true);
											setSpectator(player);	
											Manager.getArena(player).sendMessage(ChatColor.DARK_RED + player.getName() + "(" + Manager.getArena(player).getKitName(player) + ")" + ChatColor.RED + " got Blown up by a Creeper!");
											Manager.getArena(player).playSound(Sound.ENTITY_LIGHTNING_BOLT_THUNDER);
										}
									}
								}
								// Checking if a Wolf killed player
								else if(event.getDamager() instanceof Wolf)
								{
									if(((Wolf) event.getDamager()).getCustomName() != null)
									{
										String wolfName = ((Wolf) event.getDamager()).getCustomName();
										if(Manager.getArena(player).getPlayers().contains(Bukkit.getPlayer(wolfName).getUniqueId()))
										{
											e.setCancelled(true);
											killPlayer(player, Bukkit.getPlayer(wolfName));
										}
										else
										{
											e.setCancelled(true);
											setSpectator(player);
											Manager.getArena(player).sendMessage(ChatColor.DARK_RED + player.getName() + "(" + Manager.getArena(player).getKitName(player) + ")" + ChatColor.RED + " was Slain by a WOLF!");
											Manager.getArena(player).playSound(Sound.ENTITY_LIGHTNING_BOLT_THUNDER);
										}
									}
									else
									{
										e.setCancelled(true);
										setSpectator(player);	
										Manager.getArena(player).sendMessage(ChatColor.DARK_RED + player.getName() + "(" + Manager.getArena(player).getKitName(player) + ")" + ChatColor.RED + " got Slain by a WOLF!");
										Manager.getArena(player).playSound(Sound.ENTITY_LIGHTNING_BOLT_THUNDER);
									}
								}
								else
								{
									e.setCancelled(true);
									setSpectator(player);	
									Manager.getArena(player).sendMessage(ChatColor.DARK_RED + player.getName() + "(" + Manager.getArena(player).getKitName(player) + ")" + ChatColor.RED + " got killed by them Monsters!");
									Manager.getArena(player).playSound(Sound.ENTITY_LIGHTNING_BOLT_THUNDER);
								}
							}
						}
						catch(Exception exception)
						{
							
							e.setCancelled(true);
							setSpectator(player);
							Manager.getArena(player).sendMessage(ChatColor.DARK_RED + player.getName() + "(" + Manager.getArena(player).getKitName(player) + ")" + ChatColor.RED + " got killed by them Nature!");
							Manager.getArena(player).playSound(Sound.ENTITY_LIGHTNING_BOLT_THUNDER);
						}	
					}
					else
					{
						e.setCancelled(true);
					}
				}
			}else
			{
				e.setCancelled(true);
			}
		}
	}
	
	
	// NON EVENT FUNCTIONS
	public void killPlayer(Player player, Player damager)
	{
		setSpectator(player);	
		Manager.getArena(damager).updateKills(damager); // we update the player kills in the hashmap
		Manager.getArena(damager).updateScoreboardKills(damager); // update the scoreboard for that player
		playerDataYML.updatePlayerKills(damager.getUniqueId());
		playerDataYML.updatePlayerBalance(damager.getUniqueId(), 300);
		
		// Custom Death Message
		Manager.getArena(player).sendMessage(ChatColor.DARK_RED + player.getName() + "(" + Manager.getArena(player).getKitName(player) + ") "+ ChatColor.RED + "was murdered in cold blood by " + ChatColor.DARK_RED + damager.getName() + "(" + Manager.getArena(damager).getKitName(damager) + ")."  );
		Manager.getArena(player).playSound(Sound.ENTITY_LIGHTNING_BOLT_THUNDER);
	}
	
	public void setSpectator(Player player)
	{
		Manager.getArena(player).setPlayerDead(player);
        try
        {
            for (ItemStack itemStack : player.getInventory().getContents()) {
                player.getWorld().dropItem(player.getLocation(), itemStack);
                player.getInventory().removeItem(itemStack);
            }
            for (ItemStack itemStack : player.getInventory().getArmorContents()) {
                player.getWorld().dropItem(player.getLocation(), itemStack);
                player.getInventory().removeItem(itemStack);
            }
        }
        catch(IllegalArgumentException e)
        {
             
        }
		 // Get arena where player died and set it to dead in the hashmap
		// Removing Potion effects
		for (PotionEffect effect : player.getActivePotionEffects())
	        player.removePotionEffect(effect.getType());
		player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20, 0));
		player.setAllowFlight(true);
		player.setFlying(true);
		player.setInvisible(true);
		player.setHealth(20);
		player.setFoodLevel(20);
		player.teleport(player.getLocation().add(0,100,0));
		// Making already targeting entities not target us 
		/*for (int i = 0; i < player.getNearbyEntities(10, 10, 10).size(); i++){
			Entity entity = player.getNearbyEntities(10, 10, 10).get(i);
			if (entity instanceof Creature){
				if (((Creature) entity).getTarget() != null && (Player) ((Creature) entity).getTarget() == player){
					// Checks if Beastmaster Wolf killed target
					if(entity instanceof Wolf)
					{
						Wolf wolf = (Wolf) entity;
						if(wolf.getCustomName()!= null && wolf.getCustomName() != "Wolf")
						{
							String customName = wolf.getCustomName();
							wolf = (Wolf)player.getWorld().spawn(entity.getLocation(), entity.getClass());
							wolf.setCustomName(customName);
							wolf.setOwner(Bukkit.getPlayer(customName));
							entity.remove();
						}
						else
						{
							player.getWorld().spawn(entity.getLocation(), entity.getClass());
							entity.remove();
						}
					}
					else
					{
						player.getWorld().spawn(entity.getLocation(), entity.getClass());
						entity.remove();
					}
				}
			}
		}*/
		// Vanishing the player
		for(UUID uuid: Manager.getArena(player).getPlayers())
		{
			Bukkit.getPlayer(uuid).hidePlayer(Main.getInstance(), player);
		}
		
		player.sendMessage(ChatColor.YELLOW + "You are a spectator now! Don't get too close to other players!");
		ItemStack compass = new ItemStack(Material.COMPASS);
		ItemMeta compassMeta = compass.getItemMeta();
		compassMeta.setDisplayName(ChatColor.RED + "Spectator Navigator");
		compass.setItemMeta(compassMeta);
		player.getInventory().addItem(compass);
	}
	
}
