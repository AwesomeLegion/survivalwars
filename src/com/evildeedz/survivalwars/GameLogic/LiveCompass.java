package com.evildeedz.survivalwars.GameLogic;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.evildeedz.survivalwars.Manager;

public class LiveCompass implements Listener{

	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(player.getInventory().getItemInMainHand() != null || player.getInventory().getItemInOffHand() != null)
			{
				 // Compass for Nearest Player
				if(e.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.COMPASS) && Manager.getArena(player).getPlayerAliveStatus(player) == 1 || e.getPlayer().getInventory().getItemInOffHand().getType().equals(Material.COMPASS) && Manager.getArena(player).getPlayerAliveStatus(player) == 1)
				{
					//Compass points to nearest player
					Player playerNear = getNearestPlayer(player);
					if (playerNear != null) {
					    player.setCompassTarget(playerNear.getLocation());
					    player.sendMessage(ChatColor.GREEN + "Your compass is now pointing at the last known location of " + ChatColor.AQUA + playerNear.getName());
					}
				}
			}
		}
	}
	
	public Player getNearestPlayer(Player player) {
	    double distNear = 0.0D;
	    Player playerNear = null;
	    for (UUID uuid : Manager.getArena(player).getPlayers()) {
	    
	    	Player player2 = Bukkit.getPlayer(uuid);
	    	if(Manager.getArena(player2).getPlayerAliveStatus(player2) == 1)
	    	{
	    		if (player == player2) { continue; }
	    		
	    		Location location2 = player.getLocation();
	    		double dist = player2.getLocation().distance(location2);
	    		if (playerNear == null || dist < distNear) {
	    			playerNear = player2;
	    			distNear = dist;
	    		}
	    	}
	    }
	    return playerNear;
	}
}
