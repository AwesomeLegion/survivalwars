package com.evildeedz.survivalwars.GameLogic;

import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Phantom;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.evildeedz.survivalwars.Config;
import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.Manager;
import com.evildeedz.survivalwars.playerDataYML;

public class GameListener implements Listener {
	
		@EventHandler
	    public void onWeatherChange(WeatherChangeEvent event) {
	     
	        boolean rain = event.toWeatherState();
	        if(rain)
	            event.setCancelled(true);
	    }
	 
	    @EventHandler
	    public void onThunderChange(ThunderChangeEvent event) {
	     
	        boolean storm = event.toThunderState();
	        if(storm)
	            event.setCancelled(true);
	    }
	    
		// WORLD LOAD
		@EventHandler
		public void onWorldLoad(WorldLoadEvent e){
			if(Config.isArenaWorld(e.getWorld()))
			{
				int arenaID = Config.getArenaIDFromWorld(e.getWorld().getName());
				Manager.getArena(arenaID).setJoinState(true);
				Manager.getArena(arenaID).updateSign(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "=> ARENA " + arenaID + "<=", ChatColor.GREEN + "Recruiting", ChatColor.BLACK.toString() + Manager.getArena(arenaID).getPlayers().size() + "/20", ChatColor.GREEN + "Click To Join");
			}
		}
		
		// PLAYER CHAT
		@EventHandler
		public void onPlayerChat(AsyncPlayerChatEvent e)
		{
			e.setCancelled(true);
			Player player = e.getPlayer();
			if(Manager.isPlaying(player))
			{
				if(Manager.getArena(player).getPlayerAliveStatus(player) == 1)
				{
					if(player.hasPermission("group.owner"))
					{
						Manager.getArena(player).sendMessage(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "OWNER " + ChatColor.WHITE.toString() + ChatColor.BOLD + player.getName() + ChatColor.GRAY + " >> " + ChatColor.WHITE + e.getMessage() );
					}
					else if(player.hasPermission("group.coowner"))
					{
						Manager.getArena(player).sendMessage(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "CO OWNER " + ChatColor.WHITE.toString() + ChatColor.BOLD + player.getName() + ChatColor.GRAY + " >> " + ChatColor.WHITE + e.getMessage() );
					}
					else if(player.hasPermission("group.admin"))
					{
						Manager.getArena(player).sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "ADMIN " + ChatColor.WHITE.toString() + ChatColor.BOLD + player.getName() + ChatColor.GRAY + " >> " + ChatColor.WHITE + e.getMessage() );
					}
					else if(player.hasPermission("group.default"))
					{
						Manager.getArena(player).sendMessage(ChatColor.WHITE + player.getName() + ChatColor.GRAY + " >> " + ChatColor.WHITE + e.getMessage());						
					}
				}
				else
				{
					for(UUID uuid: Manager.getArena(player).getPlayers())
					{
						if(Manager.getArena(player).getPlayerAliveStatus(Bukkit.getPlayer(uuid)) == 0)
						{
							if(player.hasPermission("group.owner"))
							{
								Bukkit.getPlayer(uuid).sendMessage(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "OWNER " + ChatColor.GRAY.toString() + ChatColor.BOLD + player.getName() + ChatColor.GRAY + " >> " + ChatColor.GRAY + e.getMessage() );
							}
							else if(player.hasPermission("group.coowner"))
							{
								Bukkit.getPlayer(uuid).sendMessage(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "CO OWNER " + ChatColor.GRAY.toString() + ChatColor.BOLD + player.getName() + ChatColor.GRAY + " >> " + ChatColor.GRAY + e.getMessage() );
							}
							else if(player.hasPermission("group.admin"))
							{
								Bukkit.getPlayer(uuid).sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "ADMIN " + ChatColor.GRAY.toString() + ChatColor.BOLD + player.getName() + ChatColor.GRAY + " >> " + ChatColor.GRAY + e.getMessage() );
							}
							else if(player.hasPermission("group.default"))
							{
								Bukkit.getPlayer(uuid).sendMessage(ChatColor.GRAY + player.getName() + ChatColor.GRAY + " >> " + ChatColor.GRAY + e.getMessage());						
							}
						}
					}
				}
			}
			else
			{
				for(Player p : player.getWorld().getPlayers())
				{
					if(player.hasPermission("group.owner"))
					{
						p.sendMessage(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "OWNER " + ChatColor.WHITE.toString() + ChatColor.BOLD + player.getName() + ChatColor.GRAY + " >> " + ChatColor.WHITE + e.getMessage() );
					}
					else if(player.hasPermission("group.coowner"))
					{
						p.sendMessage(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "CO OWNER " + ChatColor.WHITE.toString() + ChatColor.BOLD + player.getName() + ChatColor.GRAY + " >> " + ChatColor.WHITE + e.getMessage() );
					}
					else if(player.hasPermission("group.admin"))
					{
						p.sendMessage(ChatColor.RED.toString() + ChatColor.BOLD + "ADMIN " + ChatColor.WHITE.toString() + ChatColor.BOLD + player.getName() + ChatColor.GRAY + " >> " + ChatColor.WHITE + e.getMessage() );
					}
					else if(player.hasPermission("group.default"))
					{
						p.sendMessage(ChatColor.WHITE + player.getName() + ChatColor.GRAY + " >> " + ChatColor.WHITE + e.getMessage());						
					}
				}
			}
		}
		
		// PICKING ITEM
		@EventHandler
		public void onPickup(EntityPickupItemEvent e)
		{
			if(e.getEntity() instanceof Player)
			{
				Player player = (Player) e.getEntity();
				if(Manager.isPlaying(player))
				{
					if(Manager.getArena(player).getState() == GameState.RECRUITING || Manager.getArena(player).getState() == GameState.COUNTDOWN || Manager.getArena(player).getPlayerAliveStatus(player) == 0)
					{
						e.setCancelled(true);
					}
				}
			}
			
		}

		// DROPPING ITEM
		@EventHandler
		public void onDrop(PlayerDropItemEvent e) {
			
			Player player = e.getPlayer();
			
			if(Manager.isPlaying(player))
			{
				if(Manager.getArena(player).getState() == GameState.COUNTDOWN || Manager.getArena(player).getState() == GameState.RECRUITING || Manager.getArena(player).getPlayerAliveStatus(player) == 0)
				{
					e.setCancelled(true);
				}
			}else
			{ 
				if(!player.hasPermission("group.owner") && !player.hasPermission("group.admin") && !player.hasPermission("group.coowner"))
				{					
					e.setCancelled(true);
				}
			}
			
		}	
		
		// PLACING BLOCK
		@EventHandler
		public void onBlockPlace(BlockPlaceEvent e)
		{
			Player player = e.getPlayer();
			
			if(!player.hasPermission("group.owner") && !player.hasPermission("group.admin") && !player.hasPermission("group.coowner") && player != null)
			{	
				if(Manager.isPlaying(player))
				{
					if(Manager.getArena(player).getState() == GameState.COUNTDOWN || Manager.getArena(player).getState() == GameState.RECRUITING || Manager.getArena(player).getPlayerAliveStatus(player) == 0)
					{
						e.setCancelled(true);
					}
				}else
				{
					e.setCancelled(true);
				}
			}
			
			if(e.getBlockPlaced().getType() == Material.ENDER_CHEST && Manager.getArena(player).getGame().getIsFeastState())
			{
				
				// Items To Give on Feast
				player.getEnderChest().clear();
				Random rand = new Random();
				int randInt =1+ rand.nextInt(3);
				switch(randInt)
				{
					case 1:
						// Hulk FEAST
						// Diamond Sword Sharp IV
						// Gold Apple 3
						// Strength II Pot 3
						// Netherite Armor Full Protec III
						// Coocked Steak 64
						player.sendMessage(ChatColor.YELLOW + "You have recieved the " + ChatColor.BLUE.toString() + ChatColor.BOLD + "HULK FEAST");
						ItemStack steakHulk = new ItemStack(Material.COOKED_BEEF, 64);
						ItemStack diamondSwordHulk = new ItemStack(Material.DIAMOND_SWORD);
						diamondSwordHulk.addEnchantment(Enchantment.DAMAGE_ALL, 4);
						ItemStack netheriteHelmetHulk = new ItemStack(Material.NETHERITE_HELMET);
						netheriteHelmetHulk.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
						ItemStack netheriteChestplateHulk = new ItemStack(Material.NETHERITE_CHESTPLATE);
						netheriteChestplateHulk.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
						ItemStack netheriteLeggingHulk = new ItemStack(Material.NETHERITE_LEGGINGS);
						netheriteLeggingHulk.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
						ItemStack netheriteBootHulk = new ItemStack(Material.NETHERITE_BOOTS);
						netheriteBootHulk.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
						ItemStack goldenAppleHulk = new ItemStack(Material.GOLDEN_APPLE, 3);
						ItemStack strengthIIHulk = new ItemStack(Material.POTION, 3);
						PotionMeta strengthIIHulkMeta = (PotionMeta) strengthIIHulk.getItemMeta();
						strengthIIHulkMeta.addCustomEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 320,1), true);
						strengthIIHulkMeta.setDisplayName("Strength II (16 Seconds)");
						strengthIIHulk.setItemMeta(strengthIIHulkMeta);
						player.getEnderChest().addItem(diamondSwordHulk);
						player.getEnderChest().addItem(netheriteHelmetHulk);
						player.getEnderChest().addItem(netheriteChestplateHulk);
						player.getEnderChest().addItem(netheriteLeggingHulk);
						player.getEnderChest().addItem(netheriteBootHulk);
						player.getEnderChest().addItem(steakHulk);
						player.getEnderChest().addItem(goldenAppleHulk);
						player.getEnderChest().addItem(strengthIIHulk);
						break;
					case 2:
						// HAWKEYE FEAST
						// Iron Sword Sharp II
						// Bow Power IV Punch II 
						// 48 Arrows
						// Swiftness Pots 3
						// Diamond Helmet + Netherite Chestplate + Leggings + Armor Portec III
						// 64 Steak
						ItemStack steakHawk = new ItemStack(Material.COOKED_BEEF, 64);
						player.sendMessage(ChatColor.YELLOW + "You have recieved the " + ChatColor.BLUE.toString() + ChatColor.BOLD + "HAWKEYE FEAST");
						ItemStack ironSwordHawk = new ItemStack(Material.IRON_SWORD);
						ironSwordHawk.addEnchantment(Enchantment.DAMAGE_ALL, 2);
						ItemStack bowHawk = new ItemStack(Material.BOW);
						bowHawk.addEnchantment(Enchantment.ARROW_DAMAGE, 4);
						bowHawk.addEnchantment(Enchantment.ARROW_KNOCKBACK, 2);
						ItemStack arrowHawk = new ItemStack(Material.ARROW, 48);
						ItemStack diamondHelmetHawk = new ItemStack(Material.DIAMOND_HELMET);
						diamondHelmetHawk.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
						ItemStack netheriteChestplateHawk = new ItemStack(Material.NETHERITE_CHESTPLATE);
						netheriteChestplateHawk.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
						ItemStack diamondLeggingHawk = new ItemStack(Material.DIAMOND_LEGGINGS);
						diamondLeggingHawk.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
						ItemStack diamondBootHawk = new ItemStack(Material.DIAMOND_BOOTS);
						diamondBootHawk.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
						ItemStack swiftIIHawk = new ItemStack(Material.POTION, 3);
						PotionMeta swiftIIHawkMeta = (PotionMeta) swiftIIHawk.getItemMeta();
						swiftIIHawkMeta.addCustomEffect(new PotionEffect(PotionEffectType.SPEED, 320,1), true);
						swiftIIHawkMeta.setDisplayName("Swiftness II (16 Seconds)");
						swiftIIHawk.setItemMeta(swiftIIHawkMeta);
						player.getEnderChest().addItem(ironSwordHawk);
						player.getEnderChest().addItem(bowHawk);
						player.getEnderChest().addItem(arrowHawk);
						player.getEnderChest().addItem(diamondHelmetHawk);
						player.getEnderChest().addItem(netheriteChestplateHawk);
						player.getEnderChest().addItem(diamondLeggingHawk);
						player.getEnderChest().addItem(diamondBootHawk);
						player.getEnderChest().addItem(swiftIIHawk);
						player.getEnderChest().addItem(steakHawk);
						break;
					case 3:
						// Sneaky FEAST
						// Iron Sword Sharp V
						// Steak 64
						// Invis pots 6
						// EnderPearls 16
						// Iron Armor IV
						player.sendMessage(ChatColor.YELLOW + "You have recieved the " + ChatColor.BLUE.toString() + ChatColor.BOLD + "SNEAKY FEAST");
						ItemStack steakSneak = new ItemStack(Material.COOKED_BEEF, 64);
						ItemStack ironSwordSneak = new ItemStack(Material.IRON_SWORD);
						ironSwordSneak.addEnchantment(Enchantment.DAMAGE_ALL, 5);
						ItemStack ironHelmetSneak = new ItemStack(Material.IRON_HELMET);
						ironHelmetSneak.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
						ItemStack ironChestplateSneak = new ItemStack(Material.IRON_CHESTPLATE);
						ironChestplateSneak.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
						ItemStack ironLeggingSneak = new ItemStack(Material.IRON_LEGGINGS);
						ironLeggingSneak.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
						ItemStack ironBootSneak = new ItemStack(Material.IRON_BOOTS);
						ironBootSneak.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
						ItemStack enderPearlSneak = new ItemStack(Material.ENDER_PEARL, 16);
						ItemStack invisIISwift = new ItemStack(Material.POTION, 6);
						PotionMeta invisIISwiftMeta = (PotionMeta) invisIISwift.getItemMeta();
						invisIISwiftMeta.addCustomEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 320,0), true);
						invisIISwiftMeta.setDisplayName("Invisiblity (16 Seconds)");
						invisIISwift.setItemMeta(invisIISwiftMeta);
						player.getEnderChest().addItem(ironSwordSneak);
						player.getEnderChest().addItem(ironHelmetSneak);
						player.getEnderChest().addItem(ironChestplateSneak);
						player.getEnderChest().addItem(ironLeggingSneak);
						player.getEnderChest().addItem(ironBootSneak);
						player.getEnderChest().addItem(enderPearlSneak);
						player.getEnderChest().addItem(invisIISwift);
						player.getEnderChest().addItem(steakSneak);
						break;
				}
			}
		}
		
		// BREAKING BLOCK
		@EventHandler
		public void onBlockBreak(BlockBreakEvent e)
		{
			Player player = e.getPlayer();
			
			if(!player.hasPermission("group.owner") && !player.hasPermission("group.admin") && !player.hasPermission("group.coowner") && player != null)
			{
				if(Manager.isPlaying(player))
				{
					if(Manager.getArena(player).getState() == GameState.COUNTDOWN || Manager.getArena(player).getState() == GameState.RECRUITING || Manager.getArena(player).getPlayerAliveStatus(player) == 0)
					{
						e.setCancelled(true);
					}
				}else
				{
					e.setCancelled(true);
				}
			}
			
			if(Manager.isPlaying(player))
			{
				if(e.getBlock().getType() == Material.ENDER_CHEST && Manager.getArena(player).getGame().getIsFeastState())
				{
					e.setCancelled(true);
				}
			}
		}
		
		// HUNGER CHANGE
		@EventHandler
		public void onFoodLevelChange(FoodLevelChangeEvent e) {
			
			if(e.getEntity() instanceof Player)
			{
				Player player = (Player) e.getEntity();
				if(Manager.isPlaying(player))
				{
					if(Manager.getArena(player).getState() == GameState.COUNTDOWN || Manager.getArena(player).getState() == GameState.RECRUITING || Manager.getArena(player).getState() == GameState.ENDING || Manager.getArena(player).getState() == GameState.PRELIVE || Manager.getArena(player).getPlayerAliveStatus(player) == 0)
					{
						e.setCancelled(true);
					}
				}else
				{
					e.setCancelled(true);
				}
			}
			
		}
		
		// ENTITY SPAWN EVENT
		@EventHandler
		public void onEntitySpawn(EntitySpawnEvent e)
		{
			if(!(e.getEntity() instanceof Player) && !(e.getEntity() instanceof ArmorStand))
			{
				int arenaID = Config.getArenaIDFromWorld(e.getEntity().getWorld().getName());
				if(arenaID != 1000)
				{
					if(Manager.getArena(arenaID).getState() == GameState.COUNTDOWN || Manager.getArena(arenaID).getState() == GameState.RECRUITING || Manager.getArena(arenaID).getState() == GameState.ENDING)
					{
						e.setCancelled(true);
					}
				}else
				{
					e.setCancelled(true);
				}
				
				if(e.getEntity() instanceof Phantom)
				{
					e.setCancelled(true);
				}
			}
		}
		
		
		// ENTITY TARGET EVENT
		@EventHandler
		public void onTarget(EntityTargetEvent e)
		{
			// For this Handler Entity = Targeter & e.getTarget = Target
			
			if(e.getTarget() instanceof Player)
			{	
				Player player = (Player) e.getTarget();
				if(Manager.isPlaying(player))
				{
					// Set targeting spectators disabled
					if(Manager.getArena(player).getPlayerAliveStatus(player) == 0)
					{
						e.setCancelled(true);
					}
					// King of Creepers Target
					if(e.getEntity() instanceof Creeper && Manager.getArena(player).getKitName(player).equals("kingofcreepers"))
					{
						e.setCancelled(true);
					}
				}
			}
		}
			
		// Entity Movement Event
		@EventHandler
		public void onMove(PlayerMoveEvent e)
		{
			Player player = e.getPlayer();
			if(Manager.isPlaying(player))
			{	
				//Teleporting Spectator WHEN THEY COME IN ANOTHER PLAYERS RANGE
				if(Manager.getArena(player).getPlayerAliveStatus(player) == 0)
				{
					for(Entity entity: player.getNearbyEntities(5,5,5))
					{
						if(entity instanceof Player)
						{
							Player p = (Player) entity;
							if(Manager.getArena(p).getPlayerAliveStatus(p) == 1)
							{
								player.teleport(player.getLocation().add(0,100,0));
								player.sendMessage(ChatColor.DARK_RED + "DO NOT GET TOO CLOSE TO OTHER PLAYERS!");
							}
						}
					}
				}//Teleporting Spectator WHEN ANOTHER PLAYERS COME IN THEIR RANGE
				else if(Manager.getArena(player).getPlayerAliveStatus(player) == 1)
				{
					for(Entity entity: player.getNearbyEntities(5,5,5))
					{
						if(entity instanceof Player)
						{
							Player p = (Player) entity;
							if(Manager.getArena(p).getPlayerAliveStatus(p) == 0)
							{
								p.teleport(player.getLocation().add(0,100,0));
								p.sendMessage(ChatColor.DARK_RED + "DO NOT GET TOO CLOSE TO OTHER PLAYERS!");
							}
						}
					}
				}
			}
		}
		
		
		// PLAYER JOIN
		@EventHandler
		public void OnJoin(PlayerJoinEvent e)
		{
			e.setJoinMessage("");
			Player player = e.getPlayer();
			player.teleport(Config.getLobbySpawn());
			player.getInventory().clear();
			player.setHealth(20);
			player.setFoodLevel(20);
			player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
			player.setAllowFlight(false);
			player.setFlying(false);
			player.setInvisible(false);
			playerDataYML.doesPlayerExist(player.getUniqueId());
			
			// Removing Potion effects
			for (PotionEffect effect : player.getActivePotionEffects())
		        player.removePotionEffect(effect.getType());
			// Removing entities each time a player joins
			for(Entity entity: player.getWorld().getEntities())
			{
				if(!(entity instanceof Player) && !(entity instanceof ArmorStand))
				{
					entity.remove();
				}
			}
		}
		
		// PLAYER QUIT
		@EventHandler
		public void onQUit(PlayerQuitEvent e)
		{
			Player player = e.getPlayer();
			e.setQuitMessage("");
			if(Manager.isPlaying(player))
			{	

				Manager.getArena(player).removePlayer(player);

			}
		}		
}
