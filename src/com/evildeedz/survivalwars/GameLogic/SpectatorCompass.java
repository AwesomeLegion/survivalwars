package com.evildeedz.survivalwars.GameLogic;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import com.evildeedz.survivalwars.Manager;

public class SpectatorCompass implements Listener{

	private Inventory gui;
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = (Player) e.getPlayer();
		if(Manager.isPlaying(player))
		{
			if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
			{
				if(player.getInventory().getItemInMainHand() != null || player.getInventory().getItemInOffHand() != null)
				{
					if(player.getInventory().getItemInMainHand().getType().equals(Material.COMPASS) && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spectator Navigator") || player.getInventory().getItemInOffHand().getType().equals(Material.COMPASS) && player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(ChatColor.RED + "Spectator Navigator"))
					{
						if(Manager.getArena(player).getPlayerAliveStatus(player) == 0)
						{
							spectatorGUI(player);
						}
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e)
	{
		Player player = (Player) e.getWhoClicked();
		
		if(e.getInventory().equals(gui) && Manager.isPlaying(player))
		{
			if( Manager.getArena(player).getPlayerAliveStatus(player) == 0 )
			{
				for(Player p: player.getWorld().getPlayers())
				{
					if(e.getCurrentItem().getItemMeta().getDisplayName().equals(p.getName()))
					{
						player.teleport(p.getLocation().add(0,20,0));
						player.sendMessage(ChatColor.GREEN + "Teleproted to: " + ChatColor.YELLOW + p.getName());
						player.closeInventory();
					}
				}
				e.setCancelled(true);
			}
		}
	}
	
	public void spectatorGUI(Player player)
	{
		gui = Bukkit.createInventory(player, 36,ChatColor.RED + "Spectator Navigator");
		
		for(Player p: player.getWorld().getPlayers())
		{
			if(Manager.getArena(p).getPlayerAliveStatus(p) == 1)
			{
				ItemStack playerHead = new ItemStack(Material.PLAYER_HEAD);
				SkullMeta playerHeadMeta = (SkullMeta) playerHead.getItemMeta();
				playerHeadMeta.setOwningPlayer(p);
				playerHeadMeta.setDisplayName(p.getName());
				playerHead.setItemMeta(playerHeadMeta);
				gui.addItem(playerHead);
			}
		}
		
		player.openInventory(gui);
	}
}
