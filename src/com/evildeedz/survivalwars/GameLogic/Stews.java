package com.evildeedz.survivalwars.GameLogic;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class Stews implements Listener{
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		 // Mushroom Stew for Health & Food Regen
		if(e.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.MUSHROOM_STEW))
		{
			if(player.getFoodLevel() < 20 || player.getHealth() < 20)
			{
				// Player Health
				if(player.getHealth() <14)
				{
					player.setHealth(player.getHealth() + 6);
				}else
				{
					player.setHealth(20);
				}
				// Player Hunger
				if(player.getFoodLevel() < 14)
				{
					player.setFoodLevel(player.getFoodLevel() + 6);
				}else
				{
					player.setFoodLevel(20);
				}
				ItemStack bowl = new ItemStack(Material.BOWL);
				player.getInventory().setItemInMainHand(bowl);
				player.updateInventory();
			}			
		} 
	}
}
