package com.evildeedz.survivalwars.Kits;

import java.util.Random;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.Manager;

public class CursedEdge implements Listener{
	
	@EventHandler
	public void onHit(EntityDamageByEntityEvent e)
	{
		if(e.getEntity() instanceof Player && e.getDamager() instanceof Player)
		{
			Player player = (Player) e.getEntity();
			Player damager = (Player) e.getDamager();
			
			if(Manager.isPlaying(player) && Manager.getArena(player).getPlayerAliveStatus(player) == 1 && Manager.getArena(damager).getPlayerAliveStatus(damager)==1)
			{
				if(Manager.getArena(player).getState() == GameState.LIVE)
				{
					if(Manager.getArena(damager).getKitName(damager).equals("cursededge") && damager.getInventory().getItemInMainHand().getType().toString().toLowerCase().contains("sword"))
					{
						Random rand = new Random();
						int cursedEdgeRand = 1 + rand.nextInt(100);
						if(cursedEdgeRand <=30)
						{
							player.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 100, 1));
						}
					}
				}
			}
		} 
	}
}
