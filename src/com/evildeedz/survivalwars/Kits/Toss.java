package com.evildeedz.survivalwars.Kits;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.Manager;

public class Toss implements Listener{
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(player.getInventory().getItemInMainHand() != null || player.getInventory().getItemInOffHand() != null)
			{
				if(player.getInventory().getItemInMainHand().getType().equals(Material.STONE) && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.GRAY.toString() + ChatColor.BOLD + "Toss")|| player.getInventory().getItemInOffHand().getType().equals(Material.STONE) && player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(ChatColor.GRAY.toString() + ChatColor.BOLD + "Toss"))
				{
					if(Manager.getArena(player).getState() == GameState.PRELIVE && Manager.getArena(player).getKitName(player).equals("toss") || Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getKitName(player).equals("toss") )
					{
						if(!(Manager.getArena(player).getPlayerCooldown(player) > System.currentTimeMillis()))
						{
							e.setCancelled(true);
							player.sendMessage(ChatColor.GREEN + "You used ability " + ChatColor.GRAY.toString() + ChatColor.BOLD + "TOSS");
							player.getWorld().playSound(player.getLocation(), Sound.ENTITY_FIREWORK_ROCKET_LAUNCH, 1.0F, 1.2F);
							Manager.getArena(player).setCooldown(player, System.currentTimeMillis() + (100 * 1000));
							for(Entity entity: player.getNearbyEntities(5, 5, 5))
							{
								if(entity instanceof Player && (Player) entity != player)
								{
										((Player)entity).setVelocity(new Vector(0,1.2,0));
										((Player)entity).sendMessage(ChatColor.YELLOW + "You have been Launched into the Air by " + ChatColor.AQUA+ player.getName());
								}
							}
						}else
						{
							e.setCancelled(true);
							long longRemaining = Manager.getArena(player).getPlayerCooldown(player) - System.currentTimeMillis();
							int intRemaining = (int) longRemaining/1000;
							player.sendMessage(ChatColor.RED + "You must wait " + intRemaining + " seconds before using this ability again!");
						}
					}
					else
					{
						e.setCancelled(true);
					}
				}
			}
		}
	}
}
