package com.evildeedz.survivalwars.Kits;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.Manager;

public class Novaburst implements Listener{

	
	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(player.getInventory().getItemInMainHand() != null || player.getInventory().getItemInOffHand() != null)
			{
				if(player.getInventory().getItemInMainHand().getType().equals(Material.BLUE_ICE) && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.AQUA.toString() + ChatColor.BOLD + "Nova Burst")|| player.getInventory().getItemInOffHand().getType().equals(Material.BLUE_ICE) && player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(ChatColor.AQUA.toString() + ChatColor.BOLD + "Nova Burst"))
				{
					if(Manager.getArena(player).getState() == GameState.PRELIVE && Manager.getArena(player).getKitName(player).equals("novaburst") || Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getKitName(player).equals("novaburst") )
					{
						if(!(Manager.getArena(player).getPlayerCooldown(player) > System.currentTimeMillis()))
						{
							e.setCancelled(true);
							player.sendMessage(ChatColor.GREEN + "You used ability " + ChatColor.AQUA.toString() + ChatColor.BOLD + "NOVA BURST");
							Manager.getArena(player).setCooldown(player, System.currentTimeMillis() + (60 * 1000));
							for(Entity entity: player.getNearbyEntities(5, 5, 5))
							{
								if(entity instanceof Player && (Player) entity != player)
								{
									((Player)entity).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 120, 3));
									((Player)entity).sendMessage(ChatColor.YELLOW + "You have been Slowed by " + ChatColor.AQUA + player.getName());
								}
							}
							player.getWorld().playSound(player.getLocation(), Sound.BLOCK_SNOW_PLACE, 1.0F, 1.2F);
						}else
						{
							e.setCancelled(true);
							long longRemaining = Manager.getArena(player).getPlayerCooldown(player) - System.currentTimeMillis();
							int intRemaining = (int) longRemaining/1000;
							player.sendMessage(ChatColor.RED + "You must wait " + intRemaining + " seconds before using this ability again!");
						}
					}
					else
					{
						e.setCancelled(true);
					}
				}
			}
		}
	}
}
