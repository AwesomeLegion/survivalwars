package com.evildeedz.survivalwars.Kits;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.Main;
import com.evildeedz.survivalwars.Manager;

public class Pegasus implements Listener{
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(player.getInventory().getItemInMainHand() != null || player.getInventory().getItemInOffHand() != null)
			{
				if(player.getInventory().getItemInMainHand().getType().equals(Material.FEATHER) && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.GRAY.toString() + ChatColor.BOLD + "Fly")|| player.getInventory().getItemInOffHand().getType().equals(Material.FEATHER) && player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(ChatColor.GRAY.toString() + ChatColor.BOLD + "Fly"))
				{
					if(Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getKitName(player).equals("pegasus") || Manager.getArena(player).getState() == GameState.PRELIVE && Manager.getArena(player).getKitName(player).equals("pegasus"))
					{
						if(!(Manager.getArena(player).getPlayerCooldown(player) > System.currentTimeMillis()))
						{
							e.setCancelled(true);
							player.sendMessage(ChatColor.GREEN + "You used ability " + ChatColor.GRAY.toString() + ChatColor.BOLD + "FLY");
							Manager.getArena(player).setCooldown(player, System.currentTimeMillis() + (47 * 1000));
							player.setAllowFlight(true);
							player.setFlying(true);
							player.getWorld().playSound(player.getLocation(), Sound.ITEM_ELYTRA_FLYING, 1.0F, 1.2F);
							Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
							    @Override
							    public void run() {
							       player.sendMessage(ChatColor.GREEN + "Flight duration ended! 40 Second cooldown added!");
							       player.setFlying(false);
							       player.setAllowFlight(false);
							    }
							}, 140L);
						}else
						{
							e.setCancelled(true);
							long longRemaining = Manager.getArena(player).getPlayerCooldown(player) - System.currentTimeMillis();
							int intRemaining = (int) longRemaining/1000;
							player.sendMessage(ChatColor.RED + "You must wait " + intRemaining + " seconds before using this ability again!");
						}
					}
					else
					{
						e.setCancelled(true);
					}
				}
			}
		}
	}
}
