package com.evildeedz.survivalwars.Kits;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.Manager;

public class Beastmaster implements Listener{
		
	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(player.getInventory().getItemInMainHand() != null || player.getInventory().getItemInOffHand() != null)
			{
				if(player.getInventory().getItemInMainHand().getType().equals(Material.WOLF_SPAWN_EGG) && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.YELLOW.toString() + ChatColor.BOLD + "Spawn Wolves")|| player.getInventory().getItemInOffHand().getType().equals(Material.WOLF_SPAWN_EGG) && player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(ChatColor.YELLOW.toString() + ChatColor.BOLD + "Spawn Wolves"))
				{
					if(Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getKitName(player).equals("beastmaster") || Manager.getArena(player).getState() == GameState.PRELIVE && Manager.getArena(player).getKitName(player).equals("beastmaster"))
					{
						if(!(Manager.getArena(player).getPlayerCooldown(player) > System.currentTimeMillis()))
						{
							e.setCancelled(true);
							player.sendMessage(ChatColor.GREEN + "You used ability " + ChatColor.DARK_PURPLE.toString() + ChatColor.BOLD + "Creeper Spawn");
							Manager.getArena(player).setCooldown(player, System.currentTimeMillis() + (240 * 1000));
							Wolf wolf = (Wolf) player.getWorld().spawnEntity(player.getLocation().add(1,0,0), EntityType.WOLF);
							wolf.setOwner(player);
							wolf.setHealth(wolf.getAttribute(Attribute.GENERIC_MAX_HEALTH).getDefaultValue());
							wolf.setCustomName(player.getName());
							player.getWorld().playSound(player.getLocation(), Sound.ENTITY_WOLF_GROWL, 1.0F, 1.2F);
						}else
						{
							e.setCancelled(true);
							long longRemaining = Manager.getArena(player).getPlayerCooldown(player) - System.currentTimeMillis();
							int intRemaining = (int) longRemaining/1000;
							player.sendMessage(ChatColor.RED + "You must wait " + intRemaining + " seconds before using this ability again!");
						}
					}
					else
					{
						e.setCancelled(true);
					}
				}
			}
		}
	}

}
