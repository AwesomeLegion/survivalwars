package com.evildeedz.survivalwars.Kits;

import java.util.HashSet;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.Manager;


public class Blink implements Listener{ 
	  
	  private HashSet<Material> ignoreBlockTypes = new HashSet<Material>();
	  
	  public int maxTeleportDistance = 200;
	  	  
	  public Blink() {
		    this.ignoreBlockTypes.add(Material.AIR); 
		    this.ignoreBlockTypes.add(Material.SNOW);
		    this.ignoreBlockTypes.add(Material.TALL_GRASS);
		    this.ignoreBlockTypes.add(Material.RED_MUSHROOM);
		    this.ignoreBlockTypes.add(Material.ROSE_BUSH);
		    this.ignoreBlockTypes.add(Material.VINE);
		    this.ignoreBlockTypes.add(Material.TORCH);
		    this.ignoreBlockTypes.add(Material.REDSTONE_TORCH);
		    this.ignoreBlockTypes.add(Material.REDSTONE_WIRE);
		    this.ignoreBlockTypes.add(Material.LILY_PAD);
		    this.ignoreBlockTypes.add(Material.REDSTONE_TORCH);
	}
	  
	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(player.getInventory().getItemInMainHand() != null || player.getInventory().getItemInOffHand() != null)
			{
				if(player.getInventory().getItemInMainHand().getType().equals(Material.REDSTONE_TORCH) && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.RED.toString() + ChatColor.BOLD + "Blink")|| player.getInventory().getItemInOffHand().getType().equals(Material.REDSTONE_TORCH) && player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(ChatColor.RED.toString() + ChatColor.BOLD + "Blink"))
				{
					if(Manager.getArena(player).getState() == GameState.PRELIVE && Manager.getArena(player).getKitName(player).equals("blink") || Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getKitName(player).equals("blink") )
					{
						if(!(Manager.getArena(player).getPlayerCooldown(player) > System.currentTimeMillis()))
						{
							e.setCancelled(true);
							player.sendMessage(ChatColor.GREEN + "You used ability " + ChatColor.RED.toString() + ChatColor.BOLD + "BLINK");
							Manager.getArena(player).setCooldown(player, System.currentTimeMillis() + (30 * 1000));
							List<Block> b = player.getLastTwoTargetBlocks(this.ignoreBlockTypes, this.maxTeleportDistance);
					        if (b.size() > 1 && ((Block)b.get(1)).getType() != Material.AIR) {
					          double dist = player.getLocation().distance(((Block)b.get(0)).getLocation());
					          if (dist > 2.0D) {
					            Location loc = ((Block)b.get(0)).getLocation().clone().add(0.5D, 0.5D, 0.5D);
					            Location pLoc = player.getLocation();
					            loc.setPitch(pLoc.getPitch());
					            loc.setYaw(pLoc.getYaw());
					            player.eject();
					            player.teleport(loc);
					            pLoc.getWorld().playSound(pLoc, Sound.ENTITY_ENDERMAN_TELEPORT, 1.0F, 1.2F);
					            pLoc.getWorld().playSound(loc, Sound.ENTITY_ENDERMAN_TELEPORT, 1.0F, 1.2F);
					            pLoc.getWorld().strikeLightningEffect(loc);
					            return;
					          } 
					        } 
						}else
						{
							e.setCancelled(true);
							long longRemaining = Manager.getArena(player).getPlayerCooldown(player) - System.currentTimeMillis();
							int intRemaining = (int) longRemaining/1000;
							player.sendMessage(ChatColor.RED + "You must wait " + intRemaining + " seconds before using this ability again!");
						}
					}
					else
					{
						e.setCancelled(true);
					}
				}
			}
		}
	}
}
