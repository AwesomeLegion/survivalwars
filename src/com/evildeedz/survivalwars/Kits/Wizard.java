package com.evildeedz.survivalwars.Kits;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.Manager;

public class Wizard implements Listener{

	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(player.getInventory().getItemInMainHand() != null || player.getInventory().getItemInOffHand() != null)
			{
				if(player.getInventory().getItemInMainHand().getType().equals(Material.FIRE_CHARGE) && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.GOLD.toString() +ChatColor.BOLD + "Fireball")|| player.getInventory().getItemInOffHand().getType().equals(Material.FIRE_CHARGE) && player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(ChatColor.GOLD.toString() +ChatColor.BOLD + "Fireball"))
				{
					if(Manager.getArena(player).getState() == GameState.PRELIVE && Manager.getArena(player).getKitName(player).equals("wizard") || Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getKitName(player).equals("wizard") )
					{
						if(!(Manager.getArena(player).getPlayerCooldown(player) > System.currentTimeMillis()))
						{
							e.setCancelled(true);
							player.sendMessage(ChatColor.GREEN + "You used ability " + ChatColor.GOLD.toString() + ChatColor.BOLD + "WIZARD");
							Manager.getArena(player).setCooldown(player, System.currentTimeMillis() + (200 * 1000));
							Fireball f = player.launchProjectile(Fireball.class);
							f.setYield(4);
							f.setIsIncendiary(false);
						}else
						{
							e.setCancelled(true);
							long longRemaining = Manager.getArena(player).getPlayerCooldown(player) - System.currentTimeMillis();
							int intRemaining = (int) longRemaining/1000;
							player.sendMessage(ChatColor.RED + "You must wait " + intRemaining + " seconds before using this ability again!");
						}
					}
					else
					{
						e.setCancelled(true);
					}
				}
			}
		}
	}
}
