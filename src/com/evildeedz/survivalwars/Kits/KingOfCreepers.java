package com.evildeedz.survivalwars.Kits;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.Manager;

public class KingOfCreepers implements Listener{

	@EventHandler
	public void onHit(EntityDamageByEntityEvent e)
	{
		if(e.getDamager() instanceof Creeper && e.getEntity() instanceof Player)
		{
			Player player = (Player) e.getEntity();
			if(Manager.getArena(player).getKitName(player).equals("kingofcreepers"))
			{
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(player.getInventory().getItemInMainHand() != null || player.getInventory().getItemInOffHand() != null)
			{
				if(player.getInventory().getItemInMainHand().getType().equals(Material.CREEPER_SPAWN_EGG) && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.DARK_GREEN.toString() + ChatColor.BOLD + "Spawn Creepers")|| player.getInventory().getItemInOffHand().getType().equals(Material.CREEPER_SPAWN_EGG) && player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(ChatColor.DARK_GREEN.toString() + ChatColor.BOLD + "Spawn Creepers"))
				{
					if(Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getKitName(player).equals("kingofcreepers") || Manager.getArena(player).getState() == GameState.PRELIVE && Manager.getArena(player).getKitName(player).equals("kingofcreepers"))
					{
						if(!(Manager.getArena(player).getPlayerCooldown(player) > System.currentTimeMillis()))
						{
							e.setCancelled(true);
							player.sendMessage(ChatColor.GREEN + "You used ability " + ChatColor.DARK_PURPLE.toString() + ChatColor.BOLD + "Creeper Spawn");
							Manager.getArena(player).setCooldown(player, System.currentTimeMillis() + (300 * 1000));
							// 1st Creeper
							Creeper creeper1 = (Creeper) player.getWorld().spawnEntity(player.getLocation().add(2,0,0), EntityType.CREEPER);
							creeper1.setCustomName(player.getName().toString());
							// 2nd Creeper
							Creeper creeper2 = (Creeper) player.getWorld().spawnEntity(player.getLocation().add(-2,0,0), EntityType.CREEPER);
							creeper2.setCustomName(player.getName().toString());
							// 3rd Creeper
							Creeper creeper3 = (Creeper) player.getWorld().spawnEntity(player.getLocation().add(0,0,-2), EntityType.CREEPER);
							creeper3.setCustomName(player.getName().toString());
							// 4th Creeper
							Creeper creeper4= (Creeper) player.getWorld().spawnEntity(player.getLocation().add(0,0,2), EntityType.CREEPER);
							creeper4.setCustomName(player.getName().toString());
							player.getWorld().playSound(player.getLocation(), Sound.ENTITY_CREEPER_HURT, 1.0F, 1.2F);
						}else
						{
							e.setCancelled(true);
							long longRemaining = Manager.getArena(player).getPlayerCooldown(player) - System.currentTimeMillis();
							int intRemaining = (int) longRemaining/1000;
							player.sendMessage(ChatColor.RED + "You must wait " + intRemaining + " seconds before using this ability again!");
						}
					}
					else
					{
						e.setCancelled(true);
					}
				}
			}
		}
	}
	
	// ENTITY TARGET EVENT
	@EventHandler
	public void onTarget(EntityTargetEvent e)
	{
		
		if(e.getTarget() instanceof Player)
		{	
			Player player = (Player) e.getTarget();
			if(Manager.isPlaying(player))
			{
				// King of Creepers Target
				if(e.getEntity() instanceof Creeper && Manager.getArena(player).getKitName(player).equals("kingofcreepers"))
				{
					e.setCancelled(true);
				}
			}
		}
	}
}
