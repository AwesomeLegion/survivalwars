package com.evildeedz.survivalwars.Kits;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.Manager;

public class Blur implements Listener{

	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(player.getInventory().getItemInMainHand() != null || player.getInventory().getItemInOffHand() != null)
			{
				if(player.getInventory().getItemInMainHand().getType().equals(Material.GLASS_PANE) && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.BLUE.toString() + ChatColor.BOLD + "Blur")|| player.getInventory().getItemInOffHand().getType().equals(Material.GLASS_PANE) && player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(ChatColor.BLUE.toString() + ChatColor.BOLD + "Blur"))
				{
					if(Manager.getArena(player).getState() == GameState.PRELIVE && Manager.getArena(player).getKitName(player).equals("blur") || Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getKitName(player).equals("blur") )
					{
						if(!(Manager.getArena(player).getPlayerCooldown(player) > System.currentTimeMillis()))
						{
							e.setCancelled(true);
							player.sendMessage(ChatColor.GREEN + "You used ability " + ChatColor.BLUE.toString() + ChatColor.BOLD + "BLUR");
							Manager.getArena(player).setCooldown(player, System.currentTimeMillis() + (60 * 1000));
							player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 160, 0));
							player.getWorld().playSound(player.getLocation(), Sound.ITEM_CHORUS_FRUIT_TELEPORT, 1.0F, 1.2F);
						}else
						{
							e.setCancelled(true);
							long longRemaining = Manager.getArena(player).getPlayerCooldown(player) - System.currentTimeMillis();
							int intRemaining = (int) longRemaining/1000;
							player.sendMessage(ChatColor.RED + "You must wait " + intRemaining + " seconds before using this ability again!");
						}
					}
					else
					{
						e.setCancelled(true);
					}
				} 
			}
		}
	}
}
