package com.evildeedz.survivalwars.Kits;

import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.SpectralArrow;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.Manager;

public class FrostArrows implements Listener{

	@EventHandler
	public void onHit(EntityDamageByEntityEvent e)
	{
		if(e.getDamager() instanceof Projectile && e.getEntity() instanceof Player)
		{
			Player player = (Player) e.getEntity();
			Projectile projectile = (Projectile) e.getDamager();
			
			// If projectile shot by a player
			if(projectile.getShooter() instanceof Player)
			{
				Player damager = (Player) projectile.getShooter();
				
				// If GameState is LIVE & Reciever is ALIVE
				if(Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getPlayerAliveStatus(player) == 1)
				{
					// Frost Arrow Kit
					if(Manager.getArena(damager).getKitName(damager).equals("frostarrows") && e.getDamager() instanceof SpectralArrow)
					{
						player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 60, 2));								
					}
				}
			}
		}
	}
}
