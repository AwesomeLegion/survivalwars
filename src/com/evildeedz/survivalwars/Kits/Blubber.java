package com.evildeedz.survivalwars.Kits;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.Manager;

public class Blubber implements Listener{

	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(player.getInventory().getItemInMainHand() != null || player.getInventory().getItemInOffHand() != null)
			{
				if(player.getInventory().getItemInMainHand().getType().equals(Material.ROTTEN_FLESH) && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.DARK_PURPLE.toString() + ChatColor.BOLD + "Blubber")|| player.getInventory().getItemInOffHand().getType().equals(Material.ROTTEN_FLESH) && player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(ChatColor.DARK_PURPLE.toString() + ChatColor.BOLD + "Blubber"))
				{
					if(Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getKitName(player).equals("blubber"))
					{
						if(!(Manager.getArena(player).getPlayerCooldown(player) > System.currentTimeMillis()))
						{
							e.setCancelled(true);
							if(player.getFoodLevel()< 20)
							{
								player.sendMessage(ChatColor.GREEN + "You used ability " + ChatColor.DARK_PURPLE.toString() + ChatColor.BOLD + "BLUBBER");
								Manager.getArena(player).setCooldown(player, System.currentTimeMillis() + (30 * 1000));
								if(player.getFoodLevel() < 10)
								{
									player.setFoodLevel(player.getFoodLevel() + 10);
								}
								else{
									player.setFoodLevel(20);
								}
								player.getWorld().playSound(player.getLocation(), Sound.ENTITY_PLAYER_BURP, 1.0F, 1.2F);
							}
							else
							{
								player.sendMessage(ChatColor.RED + "You cannot use this ability, Hunger already full!");
							}
						}else
						{
							e.setCancelled(true);
							long longRemaining = Manager.getArena(player).getPlayerCooldown(player) - System.currentTimeMillis();
							int intRemaining = (int) longRemaining/1000;
							player.sendMessage(ChatColor.RED + "You must wait " + intRemaining + " seconds before using this ability again!");
						}
					}
					else
					{
						e.setCancelled(true);
					}
				}
			}
		}
	}
}
