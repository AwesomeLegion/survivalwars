package com.evildeedz.survivalwars.Kits;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.evildeedz.survivalwars.GameState;
import com.evildeedz.survivalwars.Manager;

public class Elemental implements Listener{

	@EventHandler
	public void onInteract(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			if(player.getInventory().getItemInMainHand() != null || player.getInventory().getItemInOffHand() != null)
			{	// Orb of Harmony
				if(player.getInventory().getItemInMainHand().getType().equals(Material.LIGHT_BLUE_DYE) && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.BLUE.toString() + ChatColor.BOLD + "Orb Of Harmony")|| player.getInventory().getItemInOffHand().getType().equals(Material.LIGHT_BLUE_DYE) && player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(ChatColor.BLUE.toString() + ChatColor.BOLD + "Orb Of Harmony"))
				{
					if(Manager.getArena(player).getState() == GameState.PRELIVE && Manager.getArena(player).getKitName(player).equals("elemental") || Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getKitName(player).equals("elemental") )
					{
						e.setCancelled(true);
						player.sendMessage(ChatColor.GREEN + "You used ability " + ChatColor.AQUA.toString() + ChatColor.BOLD + "ORB OF HARMONY");
						player.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
						player.removePotionEffect(PotionEffectType.SPEED);
						player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, Integer.MAX_VALUE, 0));
					}
					else
					{
						e.setCancelled(true);
					}
				}
				else if(player.getInventory().getItemInMainHand().getType().equals(Material.MAGENTA_DYE) && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "Orb Of Swiftness")|| player.getInventory().getItemInOffHand().getType().equals(Material.MAGENTA_DYE) && player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "Orb Of Swiftness"))
				{
					if(Manager.getArena(player).getState() == GameState.PRELIVE && Manager.getArena(player).getKitName(player).equals("elemental") || Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getKitName(player).equals("elemental") )
					{
						e.setCancelled(true);
						player.sendMessage(ChatColor.GREEN + "You used ability " + ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "ORB OF SWIFTNESS");
						player.removePotionEffect(PotionEffectType.REGENERATION);
						player.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
						player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1));
					}
					else
					{
						e.setCancelled(true);
					}
				}
				else if(player.getInventory().getItemInMainHand().getType().equals(Material.ORANGE_DYE) && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.RED.toString() + ChatColor.BOLD + "Overwhelming Orb")|| player.getInventory().getItemInOffHand().getType().equals(Material.ORANGE_DYE) && player.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(ChatColor.RED.toString() + ChatColor.BOLD + "Overwhelming Orb"))
				{
					if(Manager.getArena(player).getState() == GameState.PRELIVE && Manager.getArena(player).getKitName(player).equals("elemental") || Manager.getArena(player).getState() == GameState.LIVE && Manager.getArena(player).getKitName(player).equals("elemental") )
					{
						e.setCancelled(true);
						player.sendMessage(ChatColor.GREEN + "You used ability " + ChatColor.RED.toString() + ChatColor.BOLD + "OVERWHELMING ORB");
						player.removePotionEffect(PotionEffectType.REGENERATION);
						player.removePotionEffect(PotionEffectType.SPEED);
						player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 0));
					}
					else
					{
						e.setCancelled(true);
					}
				}
			}
		}
	}
}
