package com.evildeedz.survivalwars;

import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

public class Countdown extends BukkitRunnable{

	private Arena arena;
	private int seconds;
	
	public Countdown(Arena arena)
	{
		this.arena = arena;
		
	}
	
	public void begin()
	{
		seconds = Config.getCountdownSeconds();
		arena.setState(GameState.COUNTDOWN);
		this.runTaskTimer(Main.getInstance(), 0, 20);
	}
	
	@Override
	public void run() {
		if(seconds == 0)
		{
			if(arena.getPlayers().size() < Config.getRequiredPlayers())
			{
				cancel();
				arena.setState(GameState.RECRUITING);
				arena.sendMessage(ChatColor.RED + "There are too few players. Countdown stopped.");
				arena.newCountdown();
				return;
			}else {
				cancel();
				arena.Start();
				return;
			}
		}
		
		if(seconds == 180 || seconds == 120 ||seconds == 60|| seconds == 30||seconds == 15 || seconds == 10 || seconds <=5)
		{
			if(seconds == 1)
			{
				arena.sendMessage(ChatColor.AQUA + "Game will start in 1 second.");
			}
			else if(seconds ==180 )
			{
				arena.sendMessage(ChatColor.AQUA + "Game will start in 3 minutes");
			}
			else if(seconds == 120)
			{
				arena.sendMessage(ChatColor.AQUA + "Game will start in 2 minutes");
			}
			else if(seconds == 60)
			{
				arena.sendMessage(ChatColor.AQUA + "Game will start in 1 minute");
			}
			else {
				arena.sendMessage(ChatColor.AQUA + "Game will start in " + seconds + " seconds.");
			}
		}
		
		if(arena.getPlayers().size() == 0)
		{
			cancel();
			arena.setState(GameState.RECRUITING);
			arena.newCountdown();
			return;
		}
		seconds --;
	}
}
