package com.evildeedz.survivalwars;

public enum GameState {
	
	RECRUITING,
	COUNTDOWN,
	PRELIVE,
	LIVE,
	ENDING;
}
