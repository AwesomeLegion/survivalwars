package com.evildeedz.survivalwars;


import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;

public class Config {
	
	private static Main main;
	
	
	public Config(Main main)
	{
		Config.main = main;
		
		main.getConfig().options().copyDefaults();
		main.saveDefaultConfig();
	
	}

	
	public static int getRequiredPlayers()
	{
		return main.getConfig().getInt("required-players");
	}
	
	public static int getMaxPlayers()
	{
		return main.getConfig().getInt("max-players");
	}
	
	public static int getCountdownSeconds()
	{
		return main.getConfig().getInt("countdown-seconds");
	}
	
	public static int getPreliveSeconds()
	{
		return main.getConfig().getInt("prelive-seconds");
	}
	
	public static int getLiveSeconds()
	{
		return main.getConfig().getInt("live-seconds");
	}
	
	public static int getEndingSeconds()
	{
		return main.getConfig().getInt("ending-seconds");
	}
	
	public static String getLobbyWorld()
	{
		return main.getConfig().getString("lobby-spawn.world");
	}
	
	// LOBBY SPAWN
	public static Location getLobbySpawn()
	{
		return new Location(
				Bukkit.getWorld(main.getConfig().getString("lobby-spawn.world")),
				main.getConfig().getDouble("lobby-spawn.x"),
				main.getConfig().getDouble("lobby-spawn.y"),
				main.getConfig().getDouble("lobby-spawn.z"),
				main.getConfig().getInt("lobby-spawn.yaw"),
				main.getConfig().getInt("lobby-spawn.pitch ")
				);
	}
	
	// ARENA SPAWN
	public static Location getArenaSpawn(int id)
	{
		
		World world = Bukkit.createWorld(new WorldCreator(main.getConfig().getString("arenas."+id+".spawn.world")));
		world.setAutoSave(false);
		return new Location(
				world,
				main.getConfig().getDouble("arenas."+id+".spawn.x"),
				main.getConfig().getDouble("arenas."+id+".spawn.y"),
				main.getConfig().getDouble("arenas."+id+".spawn.z"),
				main.getConfig().getInt("arenas."+id+".spawn.yaw"),
				main.getConfig().getInt("arenas."+id+".spawn.pitch")
				);
	}
	
	// GET ARENA AMOUNT
	public static int getArenaAmount()
	{
		return main.getConfig().getConfigurationSection("arenas.").getKeys(false).size();
	}
	
	// ARENA SIGN LOCATION
	public static Location getArenaSign(int id)
	{
		return new Location(Bukkit.getWorld(main.getConfig().getString("arenas."+id+".sign.world")),
				main.getConfig().getDouble("arenas."+id+".sign.x"),
				main.getConfig().getDouble("arenas."+id+".sign.y"),
				main.getConfig().getDouble("arenas."+id+".sign.z"));
	}
	
	// is Arena sign
	public static int isArenaSign(Location location)
	{
		for(int i = 0; i<= (getArenaAmount()-1); i++)
		{
			if(getArenaSign(i).equals(location))
			{
				return i;
			}
		}
		return -1;
	}
	// ARENA ID FROM WORLD
	public static int getArenaIDFromWorld(String world)
	{
		for(int i = 0;i <= (getArenaAmount() -1); i++)
		{
			if(world.equals(main.getConfig().getString("arenas."+i+".spawn.world")))
			{
				return i;
			}
		}
		return 1000;
	}
	
	// WORLD FROM ARENA ID
	public static String getWorldFromArenaID(int arenaID)
	{
		return main.getConfig().getString("arenas."+arenaID +".spawn.world");
	}
	
	// IS ARENA WORLD
	public static boolean isArenaWorld(World world)
	{
		for(String str: main.getConfig().getConfigurationSection("arenas.").getKeys(false))
		{
			if(main.getConfig().getString("arenas."+str+".spawn.world").equals(world.getName()))
			{
				return true;
			}
		}
		return false;
	}
	
	// LEADERBOARDS
	
	// KILLS
	public static Location leaderboardKills()
	{
		return new Location(
				Bukkit.getWorld(main.getConfig().getString("leaderboards.kills.world")),
				main.getConfig().getDouble("leaderboards.kills.x"),
				main.getConfig().getDouble("leaderboards.kills.y"),
				main.getConfig().getDouble("leaderboards.kills.z")
				);	
	}
	
	// WINS
	public static Location leaderboardWins()
	{
		return new Location(
				Bukkit.getWorld(main.getConfig().getString("leaderboards.wins.world")),
				main.getConfig().getDouble("leaderboards.wins.x"),
				main.getConfig().getDouble("leaderboards.wins.y"),
				main.getConfig().getDouble("leaderboards.wins.z")
				);
	}
} 
