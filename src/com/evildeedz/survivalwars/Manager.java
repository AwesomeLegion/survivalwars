package com.evildeedz.survivalwars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.entity.Player;

public class Manager {

	private static ArrayList<Arena> arenas;
	private static HashMap <String, String> message = new HashMap<> ();
	
	public Manager()
	{
		arenas = new ArrayList<>();
		
		for(int i = 0; i <= (Config.getArenaAmount()-1); i++)
		{
			arenas.add(new Arena(i));
		}
	}
	
	public static List<Arena> getArenas() {return arenas;}
	
	public static boolean isPlaying(Player player) { 
		
		for (Arena arena : arenas) {
			if(arena.getPlayers().contains(player.getUniqueId()))
			{
				return true;
			}
		}
		return false;
	}
	
	public static Arena getArena(Player player)
	{
		for(Arena arena: arenas)
		{
			if(arena.getPlayers().contains(player.getUniqueId()))
			{
				return arena;
			}
		} 
		return null;
	}
	
	public static Arena getArena(int id)
	{
		for(Arena arena: arenas)
		{
			if(arena.getID()== id)
			{
				return arena;
			}
		}
		return null;
	}
	
	public static boolean isJoinable(int id)
	{
		if(getArena(id).getState() == GameState.RECRUITING || getArena(id).getState() == GameState.COUNTDOWN)
		{
			return true;
		}
		return false;
	}
	
	
	// MESSAGE HASHMAP
	public static HashMap<String, String> getMessage() { return message; }
}
