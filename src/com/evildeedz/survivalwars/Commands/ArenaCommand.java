package com.evildeedz.survivalwars.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.evildeedz.survivalwars.Arena;
import com.evildeedz.survivalwars.Config;
import com.evildeedz.survivalwars.Manager;

public class ArenaCommand implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		
		 if(sender instanceof Player)
		 {	
			 Player player = (Player) sender;
			 
			 if(args.length==1 && args[0].equalsIgnoreCase("list"))
			 {
				 player.sendMessage(ChatColor.GREEN + "These are the available arenas:");
				 for(Arena arena: Manager.getArenas())
				 {
					 player.sendMessage(ChatColor.GREEN + " - " + arena.getID());
				 }
				 
			 }else if(args.length == 1 && args[0].equalsIgnoreCase("leave")) 
			 {
				 if(Manager.isPlaying(player))
				 {
					  Manager.getArena(player).removePlayer(player);
					  player.sendMessage(ChatColor.GREEN + "You have returned to the lobby!");
				 }else
				 {
					 player.sendMessage(ChatColor.RED + "You are not in an arena!");
				 }
			 }else if(args.length == 2 && args[0].equalsIgnoreCase("join"))
			 {
				 try {
					 int id = Integer.parseInt(args[1]);
					 if(id >= 0  && id <= (Config.getArenaAmount() -1))
					 {
						if(Manager.isJoinable(id))
						{
							// If player is in a game and tries to join another game then
							// we tell them they are already in a game
							if(!Manager.isPlaying(player))
							{
								if(Manager.getArena(id).canJoin())
								{
									Manager.getArena(id).addPlayer(player);
								}else
								{
									player.sendMessage(ChatColor.RED + "You cannot join this game right now!");
								}
							}else
							{
								player.sendMessage(ChatColor.RED + "You are already in a game! To leave type /arena leave");
							}
							
						}else {
							player.sendMessage(ChatColor.RED + "You cannot join this game right now!");
						}
					 }else
					 {
						 player.sendMessage(ChatColor.RED + "Invalid arena! See " + ChatColor.GREEN + "/arena list" + ChatColor.RED + " for list of available arenas!");
					 }
				 }catch(NumberFormatException x)
				 {
					 player.sendMessage(ChatColor.RED + "Invalid arena! See " + ChatColor.GREEN + "/arena list" + ChatColor.RED + " for list of available arenas!");
				 }
			 }else
			 {
				 player.sendMessage(ChatColor.RED + "USAGE:");
				 player.sendMessage(ChatColor.GREEN + "- /arena list");
				 player.sendMessage(ChatColor.GREEN + "- /arena join [id]");
				 player.sendMessage(ChatColor.GREEN + "- /arena leave");
			 }
		 }else
		 {
			 System.out.println("You can't use this command from Console");
		 }
		 return false;
	}
}
