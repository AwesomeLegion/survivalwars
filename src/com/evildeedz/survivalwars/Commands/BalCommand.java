package com.evildeedz.survivalwars.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.evildeedz.survivalwars.playerDataYML;

import net.md_5.bungee.api.ChatColor;

public class BalCommand implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			player.sendMessage(ChatColor.GREEN + "Current Balance: " + ChatColor.YELLOW + String.format("%.2f", playerDataYML.getPlayerBalance(player.getUniqueId())));
			
		}else
		{
			System.out.println("Cannot use this command from console");
		}
		return false;
	}
}
