package com.evildeedz.survivalwars.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.evildeedz.survivalwars.Manager;



public class LeaveCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		// TODO Auto-generated method stub
		
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			if(!(args.length >= 1))
			{
				if(Manager.isPlaying(player))
				{
					Manager.getArena(player).removePlayer(player);
					player.sendMessage(ChatColor.GREEN + "You have returned to the lobby!");
				}else
				{
					player.sendMessage(ChatColor.RED + "You are not in an arena!");
				}
			}else
			{	
				player.sendMessage(ChatColor.RED + "Incorrect usage, just type " + ChatColor.GREEN + "/leave" + ChatColor.RED + ".");
			}
		}
		else
		{
			System.out.println("You can't use this command from Console");
		}
		
		return false;
	}

}
