package com.evildeedz.survivalwars.Commands;

import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.evildeedz.survivalwars.Manager;
import net.md_5.bungee.api.ChatColor;

public class WhisperCommand implements CommandExecutor{

	// First UUID = Sender | Second UUID = Reciever
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lable, String[] args)
	{
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			if(cmd.getName().equals("whisper") )
			{
				if(args.length <2)
				{
					player.sendMessage(ChatColor.GREEN + "Usage: /whisper [player] [message]");
				}
				else if(args.length>=2)
				{
					sendMessage(player, args);
				}
			}
			else if(cmd.getName().equals("w") )
			{
				if(args.length <2)
				{
					player.sendMessage(ChatColor.GREEN + "Usage: /w [player] [message]");
				}
				else if(args.length>=2)
				{
					sendMessage(player, args);
				}
			}
			else if(cmd.getName().equals("msg") )
			{
				if(args.length <2)
				{
					player.sendMessage(ChatColor.GREEN + "Usage: /msg [player] [message]");
				}
				else if(args.length>=2)
				{
					sendMessage(player, args);
				}
			}
			else if(cmd.getName().equals("r"))
			{
				if(args.length <1)
				{
					player.sendMessage(ChatColor.GREEN + "Usage: /r [message]");
				}
				else
				{
					// IF PlAYING
					if(Manager.isPlaying(player))
					{
						// IF ALIVE
						if(Manager.getArena(player).getPlayerAliveStatus(player) == 1)
						{
							if(Manager.getMessage().containsKey(player.getName()))
							{
								for(Player p: player.getWorld().getPlayers())
								{
									if(p.getName().equals(Manager.getMessage().get(player.getName())))
									{
										if(Manager.getArena(p).getPlayerAliveStatus(p) == 1)
										{	
											String text = "";
											for(int i = 0; i< args.length; i++)
											{
												text = text + args[i] + " ";
											}
											
											player.sendMessage(ChatColor.GREEN + "[" + ChatColor.GRAY + "PM" + ChatColor.GREEN + "]" + ChatColor.WHITE + " You " + ChatColor.GREEN + "-> " + ChatColor.WHITE + p.getName() + ChatColor.GRAY + " >> " + ChatColor.GREEN + text);
											p.sendMessage(ChatColor.GREEN + "[" + ChatColor.GRAY + "PM" + ChatColor.GREEN + "]" + ChatColor.WHITE + " " + player.getName() + ChatColor.GREEN + " -> " + ChatColor.WHITE + "You" + ChatColor.GRAY + " >> " + ChatColor.GREEN + text);
											
											player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.2F);
											p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.2F);
											return false;
										}
										else
										{
											player.sendMessage(ChatColor.DARK_RED + "You cannot reply to dead players!");
											return false;
										}
									}
								}
								player.sendMessage(ChatColor.DARK_RED + Manager.getMessage().get(player.getName()) + " isn't in your Lobby anymore!");
							}
							else
							{
								player.sendMessage(ChatColor.DARK_RED + "You have no one to reply!");
							}
						}
						// IF DEAD
						else
						{
							if(Manager.getMessage().containsKey(player.getName()))
							{
								for(Player p: player.getWorld().getPlayers())
								{
									if(p.getName().equals(Manager.getMessage().get(player.getName())))
									{
										if(Manager.getArena(p).getPlayerAliveStatus(p) == 0)
										{	
											String text = "";
											for(int i = 0; i< args.length; i++)
											{
												text = text + args[i] + " ";
											}
											
											player.sendMessage(ChatColor.GREEN + "[" + ChatColor.GRAY + "PM" + ChatColor.GREEN + "]" + ChatColor.WHITE + " You " + ChatColor.GREEN + "-> " + ChatColor.WHITE + p.getName() + ChatColor.GRAY + " >> " + ChatColor.GREEN + text);
											p.sendMessage(ChatColor.GREEN + "[" + ChatColor.GRAY + "PM" + ChatColor.GREEN + "]" + ChatColor.WHITE + " " + player.getName() + ChatColor.GREEN + " -> " + ChatColor.WHITE + "You" + ChatColor.GRAY + " >> " + ChatColor.GREEN + text);
											
											player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.2F);
											p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.2F);
											return false;
										}
										else
										{
											player.sendMessage(ChatColor.DARK_RED + "You cannot reply to alive players!");
											return false;
										}
									}
								}
								player.sendMessage(ChatColor.DARK_RED + Manager.getMessage().get(player.getName()) + " isn't in your Lobby anymore!");
							}
							else
							{
								player.sendMessage(ChatColor.DARK_RED + "You have no one to reply!");
							}
						}
					}
					// IF IN LOBBY
					else
					{
						System.out.println(Manager.getMessage());
						if(Manager.getMessage().containsKey(player.getName()))
						{
							for(Player p: player.getWorld().getPlayers())
							{
								if(p.getName().equals(Manager.getMessage().get(player.getName())))
								{
									String text = "";
									for(int i = 0; i< args.length; i++)
									{
										text = text + args[i] + " ";
									}
									
									player.sendMessage(ChatColor.GREEN + "[" + ChatColor.GRAY + "PM" + ChatColor.GREEN + "]" + ChatColor.WHITE + " You " + ChatColor.GREEN + "-> " + ChatColor.WHITE + p.getName() + ChatColor.GRAY + " >> " + ChatColor.GREEN + text);
									p.sendMessage(ChatColor.GREEN + "[" + ChatColor.GRAY + "PM" + ChatColor.GREEN + "]" + ChatColor.WHITE + " " + player.getName() + ChatColor.GREEN + " -> " + ChatColor.WHITE + "You" + ChatColor.GRAY + " >> " + ChatColor.GREEN + text);
								
									player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.2F);
									p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.2F);
									return false;
								}
							}
							player.sendMessage(ChatColor.DARK_RED + Manager.getMessage().get(player.getName()) + " isn't in your Lobby anymore!");
						}
						else
						{
							player.sendMessage(ChatColor.DARK_RED + "You have no one to reply!");
						}
					}
				}
			}
		}
		else
		{
			System.out.println("This command cannot be used through a console");
		}
		
		return false;
	}
	
	// SEND MESSAGE
	public void sendMessage(Player player, String[] str)
	{
		
		// IF PLAYER IS PLAYING
		if(Manager.isPlaying(player))
		{
			// IF PLAYER IS ALIVE
			if(Manager.getArena(player).getPlayerAliveStatus(player) == 1)
			{
				for(Player p: player.getWorld().getPlayers())
				{
					if(p.getName().toLowerCase().contains(str[0].toLowerCase()))
					{
						if(Manager.getArena(p).getPlayerAliveStatus(p) == 1)
						{		
							String text = "";
							for(int i = 1; i< str.length; i++)
							{
								text = text + str[i] + " ";
							}
							
							player.sendMessage(ChatColor.GREEN + "[" + ChatColor.GRAY + "PM" + ChatColor.GREEN + "]" + ChatColor.WHITE + " You " + ChatColor.GREEN + "-> " + ChatColor.WHITE + p.getName() + ChatColor.GRAY + " >> " + ChatColor.GREEN + text);
							p.sendMessage(ChatColor.GREEN + "[" + ChatColor.GRAY + "PM" + ChatColor.GREEN + "]" + ChatColor.WHITE + " " + player.getName() + ChatColor.GREEN + " -> " + ChatColor.WHITE + "You" + ChatColor.GRAY + " >> " + ChatColor.GREEN + text);
						
							player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.2F);
							p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.2F);
							
							if(!Manager.getMessage().containsKey(player.getName()))
								Manager.getMessage().put(player.getName(), p.getName());
							else
								Manager.getMessage().replace(player.getName(), p.getName());
							
							if(!Manager.getMessage().containsKey(p.getName()))
								Manager.getMessage().put(p.getName(), player.getName());
							else
								Manager.getMessage().replace(p.getName(), player.getName());
							return;
						}
						else
						{
							player.sendMessage(ChatColor.RED + "You cannot talk to dead people!");
							return;
						}
					}
				}
				player.sendMessage(ChatColor.DARK_RED+ "Player named " + str[0] + " is not in this Arena.");
			}
			// IF PLAYER IS DEAD
			else
			{
				for(Player p: player.getWorld().getPlayers())
				{
					if(p.getName().toLowerCase().contains(str[0].toLowerCase()))
					{
						if(Manager.getArena(p).getPlayerAliveStatus(p) == 0)
						{		
							String text = "";
							for(int i = 1; i< str.length; i++)
							{
								text = text + str[i] + " ";
							}
							
							player.sendMessage(ChatColor.GREEN + "[" + ChatColor.GRAY + "PM" + ChatColor.GREEN + "]" + ChatColor.WHITE + " You " + ChatColor.GREEN + "-> " + ChatColor.WHITE + p.getName() + ChatColor.GRAY + " >> " + ChatColor.GREEN + text);
							p.sendMessage(ChatColor.GREEN + "[" + ChatColor.GRAY + "PM" + ChatColor.GREEN + "]" + ChatColor.WHITE + " " + player.getName() + ChatColor.GREEN + " -> " + ChatColor.WHITE + "You" + ChatColor.GRAY + " >> " + ChatColor.GREEN + text);
							
							player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.2F);
							p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.2F);
							
							if(!Manager.getMessage().containsKey(player.getName()))
								Manager.getMessage().put(player.getName(), p.getName());
							else
								Manager.getMessage().replace(player.getName(), p.getName());
							
							if(!Manager.getMessage().containsKey(p.getName()))
								Manager.getMessage().put(p.getName(), player.getName());
							else
								Manager.getMessage().replace(p.getName(), player.getName());
							return;
						}
						else
						{
							player.sendMessage(ChatColor.RED + "You cannot talk to players alive!");
							return;
						}
					}
				}
				player.sendMessage(ChatColor.DARK_RED+ "Player named " + str[0] + " is not in this Arena.");
			}
		}
		// IF IN LOBBY
		else
		{
			// IF PLAYER FOUND WE SEND MSG AND THEN return; OTHERWISE TELL PLAYER NO ONE FOUND
			for(Player p: player.getWorld().getPlayers())
			{
				if(p.getName().toLowerCase().contains(str[0].toLowerCase()))
				{
					String text = "";
					for(int i = 1; i< str.length; i++)
					{
						text = text + str[i] + " ";
					}
					
					player.sendMessage(ChatColor.GREEN + "[" + ChatColor.GRAY + "PM" + ChatColor.GREEN + "]" + ChatColor.WHITE + " You " + ChatColor.GREEN + "-> " + ChatColor.WHITE + p.getName() + ChatColor.GRAY + " >> " + ChatColor.GREEN + text);
					p.sendMessage(ChatColor.GREEN + "[" + ChatColor.GRAY + "PM" + ChatColor.GREEN + "]" + ChatColor.WHITE + " " + player.getName() + ChatColor.GREEN + " -> " + ChatColor.WHITE + "You" + ChatColor.GRAY + " >> " + ChatColor.GREEN + text);
				
					player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.2F);
					p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0F, 1.2F);
					
					if(!Manager.getMessage().containsKey(player.getName()))
						Manager.getMessage().put(player.getName(), p.getName());
					else
						Manager.getMessage().replace(player.getName(), p.getName());
					
					if(!Manager.getMessage().containsKey(p.getName()))
						Manager.getMessage().put(p.getName(), player.getName());
					else
						Manager.getMessage().replace(p.getName(), player.getName());
					
					System.out.println(Manager.getMessage());
					return;
				}
			}
			player.sendMessage(ChatColor.DARK_RED+ "Player named " + str[0] + " is not in this Lobby.");
		}
	}
}
