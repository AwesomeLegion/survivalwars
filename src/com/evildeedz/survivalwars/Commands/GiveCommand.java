package com.evildeedz.survivalwars.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.evildeedz.survivalwars.playerDataYML;

import net.md_5.bungee.api.ChatColor;

public class GiveCommand implements CommandExecutor{

	private Player player;
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		// KITALL
		if(args.length == 2 && args[0].equalsIgnoreCase("kitall"))
		{
			try {
				// If player sent 
				if(sender instanceof Player)
				{
					player = (Player) sender;
					if(player.hasPermission("group.owner") || player.hasPermission("group.coowner"))
					{
						playerDataYML.giveAllKits(playerDataYML.getUUIDFromName(args[1].toString()));
					}
					else
					{
						player.sendMessage(ChatColor.RED + "You Lack Permission!");
					}
				}
				// if Console
				else
				{
					playerDataYML.giveAllKits(playerDataYML.getUUIDFromName(args[1].toString()));
				}
			}
			catch(Exception e)
			{
				if(sender instanceof Player)
					((Player)sender).sendMessage(e.toString());
				
				System.out.println(e);
			}
		}
		// KITPREMIUM
		else if(args.length == 2 && args[0].equalsIgnoreCase("kitpremium"))
		{
			try {
				// If player sent 
				if(sender instanceof Player)
				{
					player = (Player) sender;
					if(player.hasPermission("group.owner") || player.hasPermission("group.coowner"))
					{
						playerDataYML.updatePlayerPremium(playerDataYML.getUUIDFromName(args[1]), "yes");
						playerDataYML.giveAllKits(playerDataYML.getUUIDFromName(args[1].toString()));
					}
					else
					{
						player.sendMessage(ChatColor.RED + "You Lack Permission!");
					}
				}
				// if Console
				else
				{
					playerDataYML.updatePlayerPremium(playerDataYML.getUUIDFromName(args[1]), "yes");
					playerDataYML.giveAllKits(playerDataYML.getUUIDFromName(args[1].toString()));
				}
			}
			catch(Exception e)
			{
				if(sender instanceof Player)
					((Player)sender).sendMessage(e.toString());
				
				System.out.println(e);
			}
		}
		// KIT
		else if(args.length == 3 && args[0].equalsIgnoreCase("kit"))
		{
			try {
				// If player sent 
				if(sender instanceof Player)
				{
					player = (Player) sender;
					if(player.hasPermission("group.owner") || player.hasPermission("group.coowner"))
					{
						playerDataYML.addKit(playerDataYML.getUUIDFromName(args[1].toString()), args[2].toString());
					}
					else
					{
						player.sendMessage(ChatColor.RED + "You Lack Permission!");
					}
				}
				// if Console
				else
				{
					playerDataYML.addKit(playerDataYML.getUUIDFromName(args[1].toString()), args[2].toString());
				}
			}
			catch(Exception e)
			{
				if(sender instanceof Player)
					((Player)sender).sendMessage(e.toString());
				
				System.out.println(e);
			}
		}
		// BAL
		else if(args.length == 3 && args[0].equalsIgnoreCase("bal") || args.length == 3 && args[0].equalsIgnoreCase("balance"))
		{
			try {
				// If player sent 
				if(sender instanceof Player)
				{
					player = (Player) sender;
					if(player.hasPermission("group.owner") || player.hasPermission("group.coowner"))
					{
						playerDataYML.updatePlayerBalance(playerDataYML.getUUIDFromName(args[1].toString()), Double.valueOf(args[2]));
					}
					else
					{
						player.sendMessage(ChatColor.RED + "You Lack Permission!");
					}
				}
				// if Console
				else
				{
					playerDataYML.updatePlayerBalance(playerDataYML.getUUIDFromName(args[1].toString()), Double.valueOf(args[2]));
				}
			}
			catch(Exception e)
			{
				if(sender instanceof Player)
					((Player)sender).sendMessage(e.toString());
				
				System.out.println(e);
			}
		}
		else
		{
			if(sender instanceof Player)
			{
				player = (Player) sender;
				if(player.hasPermission("group.owner") || player.hasPermission("group.coowner"))
				{
					player.sendMessage(ChatColor.GREEN + "Usage:");
					player.sendMessage(ChatColor.GREEN + "-/give kitall [player]");
					player.sendMessage(ChatColor.GREEN + "-/give kitpremium [player]");
					player.sendMessage(ChatColor.GREEN + "-/give kit [player] [kitname]");
					player.sendMessage(ChatColor.GREEN + "-/give bal [player] [amount]");
				}
				else
				{
					player.sendMessage(ChatColor.RED + "You Lack Permission!");
				}
			}
			// if Console
			else
			{
				System.out.println("Usage:");
				System.out.println("-/give kitall [player]");
				System.out.println("-/give kitpremium [player]");
				System.out.println("-/give kit [player] [kitname]");
				System.out.println("-/give bal [player] [amount]");			
			}
		}
		return false;
	}
}
