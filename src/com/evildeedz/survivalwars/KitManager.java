package com.evildeedz.survivalwars;


import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import org.bukkit.ChatColor;


public class KitManager {

	public void giveKits(Player player, String kitName)
	{
		switch(kitName)
		{
			case "butcher":
				player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE ,0));
				break;
			case "flash":
				player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1));
				break;
			case "wizard":
				ItemStack wizard = new ItemStack(Material.FIRE_CHARGE);
				wizard.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
				ItemMeta wizardMeta = wizard.getItemMeta();
				wizardMeta.setDisplayName(ChatColor.GOLD.toString() +ChatColor.BOLD + "Fireball");
				wizard.setItemMeta(wizardMeta);
				player.getInventory().addItem(wizard);
				break;
			case "blur":
				ItemStack blur = new ItemStack(Material.GLASS_PANE);
				blur.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
				ItemMeta blurMeta = blur.getItemMeta();
				blurMeta.setDisplayName(ChatColor.BLUE.toString() + ChatColor.BOLD + "Blur");
				blur.setItemMeta(blurMeta);
				player.getInventory().addItem(blur);
				break;
			case "viking":
				ItemStack viking = new ItemStack(Material.STONE_AXE);
				ItemMeta vikingMeta = viking.getItemMeta();
				vikingMeta.setDisplayName(ChatColor.DARK_BLUE.toString() + ChatColor.BOLD + "Viking");
				viking.setItemMeta(vikingMeta);
				viking.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
				viking.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
				player.getInventory().addItem(viking);
				break;
			case "blubber":
				ItemStack blubber = new ItemStack(Material.ROTTEN_FLESH);
				blubber.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
				ItemMeta blubberMeta = blubber.getItemMeta();
				blubberMeta.setDisplayName(ChatColor.DARK_PURPLE.toString() + ChatColor.BOLD + "Blubber");
				blubber.setItemMeta(blubberMeta);
				player.getInventory().addItem(blubber);
				break;
			case "frostarrows":
				ItemStack frostArrowsBow = new ItemStack(Material.BOW);
				ItemMeta frostArrowsBowMeta = frostArrowsBow.getItemMeta();
				frostArrowsBowMeta.setDisplayName(ChatColor.AQUA.toString() + ChatColor.BOLD + "Frost Bow");
				frostArrowsBow.setItemMeta(frostArrowsBowMeta);
				frostArrowsBow.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 2);
				frostArrowsBow.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
				player.getInventory().addItem(frostArrowsBow);
				ItemStack frostArrows = new ItemStack(Material.SPECTRAL_ARROW, 64);
				ItemMeta frostArrowsMeta = frostArrows.getItemMeta();
				frostArrowsMeta.setDisplayName(ChatColor.AQUA.toString() + ChatColor.BOLD + "Frost Arrows");
				frostArrows.setItemMeta(frostArrowsMeta);
				player.getInventory().addItem(frostArrows);
				break;
			case "kingofcreepers":
				ItemStack kingOfCreepers = new ItemStack(Material.CREEPER_SPAWN_EGG);
				ItemMeta kingOfCreepersMeta = kingOfCreepers.getItemMeta();
				kingOfCreepersMeta.setDisplayName(ChatColor.DARK_GREEN.toString() + ChatColor.BOLD + "Spawn Creepers");
				kingOfCreepers.setItemMeta(kingOfCreepersMeta);
				kingOfCreepers.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
				player.getInventory().addItem(kingOfCreepers);
				break;
			case "blink":
				ItemStack blink = new ItemStack(Material.REDSTONE_TORCH);
				ItemMeta blinkMeta = blink.getItemMeta();
				blinkMeta.setDisplayName(ChatColor.RED.toString() + ChatColor.BOLD + "Blink");
				blink.setItemMeta(blinkMeta);
				blink.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
				player.getInventory().addItem(blink);
				break;
			case "toss":
				ItemStack toss = new ItemStack(Material.STONE);
				ItemMeta tossMeta = toss.getItemMeta();
				tossMeta.setDisplayName(ChatColor.GRAY.toString() + ChatColor.BOLD + "Toss");
				toss.setItemMeta(tossMeta);
				toss.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
				player.getInventory().addItem(toss);
				break;
			case "elemental":
				ItemStack elemental1 = new ItemStack(Material.LIGHT_BLUE_DYE);
				ItemMeta elemental1Meta = elemental1.getItemMeta();
				elemental1Meta.setDisplayName(ChatColor.BLUE.toString() + ChatColor.BOLD + "Orb Of Harmony" );
				elemental1.setItemMeta(elemental1Meta);
				elemental1.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
				
				ItemStack elemental2 = new ItemStack(Material.MAGENTA_DYE);
				ItemMeta elemental2Meta = elemental2.getItemMeta();
				elemental2Meta.setDisplayName(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "Orb Of Swiftness" );
				elemental2.setItemMeta(elemental2Meta);
				elemental2.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);

				ItemStack elemental3 = new ItemStack(Material.ORANGE_DYE);
				ItemMeta elemental3Meta = elemental3.getItemMeta();
				elemental3Meta.setDisplayName(ChatColor.RED.toString() + ChatColor.BOLD + "Overwhelming Orb" );
				elemental3.setItemMeta(elemental3Meta);
				elemental3.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);

				player.getInventory().addItem(elemental1);
				player.getInventory().addItem(elemental2);
				player.getInventory().addItem(elemental3);
				break;
			case "beastmaster":
				ItemStack beastmaster = new ItemStack(Material.WOLF_SPAWN_EGG);
				ItemMeta beastmasterMeta = beastmaster.getItemMeta();
				beastmasterMeta.setDisplayName(ChatColor.YELLOW.toString() + ChatColor.BOLD + "Spawn Wolves");
				beastmaster.setItemMeta(beastmasterMeta);
				beastmaster.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
				player.getInventory().addItem(beastmaster);
				break;
			case "novaburst":
				ItemStack novaburst = new ItemStack(Material.BLUE_ICE);
				ItemMeta novaburstMeta = novaburst.getItemMeta();
				novaburstMeta.setDisplayName(ChatColor.AQUA.toString() + ChatColor.BOLD + "Nova Burst");
				novaburst.setItemMeta(novaburstMeta);
				novaburst.addUnsafeEnchantment(Enchantment.ARROW_INFINITE,1);
				player.getInventory().addItem(novaburst);
				break;
			case "pegasus":
				ItemStack pegasus = new ItemStack(Material.FEATHER);
				ItemMeta pegasusMeta = pegasus.getItemMeta();
				pegasusMeta.setDisplayName(ChatColor.GRAY.toString() + ChatColor.BOLD + "Fly");
				pegasus.setItemMeta(pegasusMeta);
				pegasus.addUnsafeEnchantment(Enchantment.ARROW_INFINITE,1);
				player.getInventory().addItem(pegasus);
				break;
			case "berserk":
				ItemStack berserk = new ItemStack(Material.GOLDEN_APPLE);
				ItemMeta berserkMeta = berserk.getItemMeta();
				berserkMeta.setDisplayName(ChatColor.DARK_RED.toString() + ChatColor.BOLD + "Go Berserk");
				berserk.setItemMeta(berserkMeta);
				berserk.addUnsafeEnchantment(Enchantment.ARROW_INFINITE,1);
				player.getInventory().addItem(berserk);
				break;
			case "smokeofdeceit":
				ItemStack smokeofdeceit = new ItemStack(Material.ENDER_EYE);
				ItemMeta smokeofdeceitMeta = smokeofdeceit.getItemMeta();
				smokeofdeceitMeta.setDisplayName(ChatColor.LIGHT_PURPLE.toString() + ChatColor.BOLD + "Go Deceit");
				smokeofdeceit.setItemMeta(smokeofdeceitMeta);
				smokeofdeceit.addUnsafeEnchantment(Enchantment.ARROW_INFINITE,1);
				player.getInventory().addItem(smokeofdeceit);
				break;
		}
	}

}
