package com.evildeedz.survivalwars;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.UUID;
import java.util.logging.Level;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class playerDataYML {
	
	private static Main main;
	private static File playerData = null;
	private static FileConfiguration playerDataConfig = null;
	
	public playerDataYML(Main main)
	{
		playerDataYML.main = main;
		saveDefaultConfig();
	}
	
	public static void reloadConfig()
	{
		if(playerData == null)
			playerData = new File(main.getDataFolder(), "playerData.yml");
		
		playerDataConfig = YamlConfiguration.loadConfiguration(playerData);
			
		InputStream defaultStream = main.getResource("playerData.yml");
		if(defaultStream != null)
		{
			YamlConfiguration defaultConig = YamlConfiguration.loadConfiguration(new InputStreamReader(defaultStream));
			playerDataConfig.setDefaults(defaultConig);
		}
	}
	
	public static FileConfiguration getConfig()
	{
		if(playerDataConfig == null)
		{
			reloadConfig();
		}
		return playerDataConfig;
	}
	
	public static void saveConfig()
	{
		if(playerDataConfig == null || playerData == null)
			return;
		
		try {
			getConfig().save(playerData);
		} catch (IOException e) {
			main.getLogger().log(Level.SEVERE, "Could not save config to " + playerData, e);
		}
	}
	
	public static void saveDefaultConfig()
	{
		if(playerData == null)
			playerData = new File(main.getDataFolder(), "playerData.yml");
		
		if(!playerData.exists())
		{
			main.saveResource("playerData.yml", false);
		}
		
	}
	
	public static void doesPlayerExist(UUID uuid)
	{
		if(!getConfig().isConfigurationSection("players."+uuid.toString()))
		{
			getConfig().createSection("players."+uuid.toString());
			getConfig().createSection("players."+uuid.toString()+".name");
			getConfig().set("players."+uuid.toString()+ ".name", Bukkit.getPlayer(uuid).getName());
			getConfig().createSection("players."+uuid.toString()+ ".bal");
			getConfig().set("players."+uuid.toString()+ ".bal", 0);
			getConfig().createSection("players."+uuid.toString()+ ".wins");
			getConfig().set("players."+uuid.toString()+ ".wins", 0);
			getConfig().createSection("players."+uuid.toString()+ ".kills");
			getConfig().set("players."+uuid.toString()+ ".kills", 0);
			getConfig().createSection("players."+uuid.toString()+ ".kits");
			getConfig().set("players."+uuid.toString()+ ".kits", "butcher-flash-frost-wizard-blur-viking-blubber");
			getConfig().createSection("players."+uuid.toString()+ ".premium");
			getConfig().set("players."+uuid.toString()+ ".premium", "no");
			saveConfig();
		}
		
		//if Player has subscribed to premium always assign them new kits on login
		if(getConfig().getString("players."+uuid.toString()+ ".premium").equals("yes"))
		{
			giveAllKits(uuid);
		}
		
		// Updating Username for the player everytime they login
		getConfig().set("players."+uuid.toString()+ ".name", Bukkit.getPlayer(uuid).getName());
		saveConfig();
	}
	
	// UPDATE KILLS
	public static void updatePlayerKills(UUID uuid)
	{
		int kills = getConfig().getInt("players."+uuid.toString()+".kills");
		getConfig().set("players."+uuid.toString()+".kills", (kills+1));
		saveConfig();
	}
	
	// UPDATE WINS
	public static void updatePlayerWins(UUID uuid)
	{
		int wins = getConfig().getInt("players."+uuid.toString()+".wins");
		getConfig().set("players."+uuid.toString()+".wins", (wins+1));
		saveConfig();
	}
	
	// UPDATE BALANCE
	public static void updatePlayerBalance(UUID uuid, double balance)
	{
		/* Each Prem Kit = 7000$
		 * Each kill = 300$
		 * Each win = 3000$
		 */
		double bal = getConfig().getDouble("players."+uuid.toString()+".bal");
		getConfig().set("players."+uuid.toString()+".bal", (bal+balance));
		saveConfig();
	}
	
	// UPDATE PREMIUM
	public static void updatePlayerPremium(UUID uuid, String str)
	{
		getConfig().set("players."+uuid.toString()+ ".premium", str);
		saveConfig();
	}
	
	// GET PLAYER BALANCE
	public static double getPlayerBalance(UUID uuid)
	{
		return getConfig().getDouble("players."+uuid.toString()+".bal");
	}
	
	// TOP 10 KILLS
	public static LinkedHashMap<String, Integer> getTopKills()
	{
		HashMap<String, Integer> killMap = new HashMap<>();
		LinkedHashMap<String, Integer> sortedKillMap = new LinkedHashMap<>();
		
		for(String str: getConfig().getConfigurationSection("players.").getKeys(false))
		{
			killMap.put(getConfig().getString("players."+str+".name"), getConfig().getInt("players."+str+".kills"));		
		}
		
		killMap.entrySet()
	    .stream()
	    .sorted(HashMap.Entry.comparingByValue(Comparator.reverseOrder())) 
	    .forEachOrdered(x -> sortedKillMap.put(x.getKey(), x.getValue()));
		return sortedKillMap;
	}
	
	// TOP 10 WINS
	public static LinkedHashMap<String, Integer> getTopWins()
	{
		HashMap<String, Integer> winMap = new HashMap<>();
		LinkedHashMap<String, Integer> sortedWinMap = new LinkedHashMap<>();
		
		for(String str: getConfig().getConfigurationSection("players.").getKeys(false))
		{
			winMap.put(getConfig().getString("players."+str+".name"), getConfig().getInt("players."+str+".wins"));		
		}
		
		winMap.entrySet()
	    .stream()
	    .sorted(HashMap.Entry.comparingByValue(Comparator.reverseOrder())) 
	    .forEachOrdered(x -> sortedWinMap.put(x.getKey(), x.getValue()));
		return sortedWinMap;
	}
	
	// KITS
	
	// DOES KIT EXIST
	public static boolean doesKitExist(String kitName)
	{
		for(KitType type: KitType.values())
		{
			if(type.getKitName().equals(kitName))
				return true;
		}
		return false;
	}
	
	// DOES PLAYER OWN KIT
	public static boolean doesPlayerOwnKit(UUID uuid, String kitName)
	{
		String ownedKits = getConfig().getString("players."+uuid.toString()+".kits");
		
		for(String name: ownedKits.split("-"))
		{
			if(kitName.equals(name))
				return true;
		}
		
		return false;
	}
	
	// ADD KIT
	public static void addKit(UUID uuid, String kitname)
	{
		if(doesKitExist(kitname) && !doesPlayerOwnKit(uuid,kitname))
		{
			String newKit = getConfig().getString("players."+uuid.toString()+".kits") + "-"+kitname;
			getConfig().set("players."+uuid.toString()+".kits", newKit);
			saveConfig();
		}
	}
	
	// GET KITS OWNED BY PLAYER
	public static String[] getPlayerKits(UUID uuid)
	{
		String[] ownedKits = getConfig().getString("players."+uuid.toString()+".kits").split("-");
		return ownedKits;
	}
	// GET UUID FROM NAME
	public static UUID getUUIDFromName(String str)
	{
		for(String ids: getConfig().getConfigurationSection("players.").getKeys(false))
		{
			if(getConfig().getString("players."+ids+".name").toLowerCase().equals(str.toLowerCase()))
			{
				return UUID.fromString(ids);
			}
		}
		return null;
	}
	// ASSIGN ALL KITS
	public static void giveAllKits(UUID uuid)
	{
		// Checking if player exists
		for(KitType type: KitType.values())
		{
			if(!doesPlayerOwnKit(uuid, type.getKitName()))
			{
				addKit(uuid,type.getKitName());
			}
		}
	}
}
